<div class="col-xs-12">  
  <div class="box">
     <div class="box-body">
        <script type="text/javascript">
            $(document).ready(function(){ 
            $("#myTab a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
                });
            });
        </script>
<p align="right"><button class="btn btn-primary" onclick="reload()"><span class="glyphicon glyphicon-refresh"></span>  Refresh</button></p>
<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#queue">Queue Tests</a></li>
    <li><a href="#result">Result</a></li>
    <li><a href="#review">Review</a></li>    
</ul> 

<div class="tab-content">

          <!--TAB 1-->
          <div id="queue"  class="tab-pane fade in active">
            <?php
              $queue=mysqli("SELECT t1.uid_tested, t3.nama_mapel, t2.nama_tes, t2.waktu, t2.status_tes, t1.status FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t1.uid_user='".$_SESSION['uid_user']."' AND (t1.status='not yet' OR t1.status='on going') ORDER BY t2.update_tes DESC");
            ?>
            <br>
            <table class="table bordered">
              <thead>
                <tr><th>No.</th><th>Mapel</th><th>Nama Tes</th><th>Waktu</th><th>Status</th></tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                while($aqueue=mysqli_fetch_array($queue)){
                  echo "<tr><td>".$no.".</td><td>".$aqueue['nama_mapel']."</td><td>".$aqueue['nama_tes']."</td><td>".$aqueue['waktu']."</td>
                  <td>";
                  if($aqueue['status_tes']=="open"){
                    if($aqueue['status']=="not yet"){
                      echo "<a href='main_page.php?view=token&uid_tested=$aqueue[uid_tested]' class='btn btn-success'><span class='glyphicon glyphicon-file'></span> Start test</a>";  
                    }elseif ($aqueue['status']=="on going") {
                      echo "<a href='main_page.php?view=token&uid_tested=$aqueue[uid_tested]' class='btn btn-danger'><span class='glyphicon glyphicon-file'></span> ter-Logout</a>";  
                    }
                    
                  }else{
                    echo "<p class='btn btn-default''><span class='glyphicon glyphicon-ban-circle'></span>&nbsp;&nbsp;&nbsp;Close</p>";
                  }
                  echo "</td></tr>";
                  $no=$no+1;
                }
                
                ?>
              </tbody>
            </table>
          </div>

          <!--TAB 2-->         
          <div id="result"  class="tab-pane fade">
              <?php
              $result=mysqli("SELECT t1.uid_tested, t3.nama_mapel, t2.nama_tes, t2.hasil_tes, t1.start_time, t1.durasi,t1.nilai FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t1.uid_user='".$_SESSION['uid_user']."' AND t1.status='finish' ORDER BY t1.end_time DESC");
            ?>
            <br>
            <table class="table bordered">
              <thead>
                <tr><th>No.</th><th>Mapel</th><th>Nama Tes</th><th>Mulai</th><th>Durasi Kerja</th><th>Nilai</th></tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                while($aresult=mysqli_fetch_array($result)){
                  echo "<tr><td>".$no.".</td><td>".$aresult['nama_mapel']."</td><td>".$aresult['nama_tes']."</td><td>".$aresult['start_time']."</td><td>".$aresult['durasi']."</td>";
                  if($aresult['hasil_tes']=="0"){
                    echo "<td style='color:red;'>Nilai tidak diperlihatkan</td>";  
                  }else{
                    echo "<td style='font-size:24pt; color:blue;'><b>".$aresult['nilai']."</b></td>";
                  }
                  echo "</tr>";
                  $no=$no+1;
                }
                
                ?>
              </tbody>
            </table>

          </div>

                    <!--TAB 3-->
          <div id="review"  class="tab-pane fade">
              <?php
              $result=mysqli("SELECT t1.uid_tested, t3.nama_mapel, t2.nama_tes, t2.uid_tes FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t1.uid_user='".$_SESSION['uid_user']."' AND t1.status='finish' AND t2.status_review='open' ORDER BY t1.end_time ASC");
            ?>
            <br>
            <table class="table bordered">
              <thead>
                <tr><th>No.</th><th>Mapel</th><th>Nama Tes</th><th>Review</th></tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                while($aresult=mysqli_fetch_array($result)){
                  echo "<tr><td>".$no.".</td><td>".$aresult['nama_mapel']."</td><td>".$aresult['nama_tes']."</td><td><a href='main_page.php?view=review&dat=".$aresult['uid_tested']."' class='btn btn-success'><span class='glyphicon glyphicon-search'></span> Review</a></td></tr>";
                  $no=$no+1;
                }
                
                ?>
              </tbody>
            </table>

          </div>

</div> 
</div>
</div><!-- /.box-body -->
</div>
<script type="text/javascript">
  function reload(){
    window.location.reload();
    //var current_index = $("#myTab").tabs("option","selected");
    //$("#myTab").tabs('load',current_index);
  }
</script>