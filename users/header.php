<nav class="navbar navbar-default" style="background: #367FA9;">
  <div style="width: 100%;">
    <div style="width: 30%; float: left;">
      <div class="navbar-brand" style="color: #FFFB23;font-size: 20px;font-weight: bold;font-style: italic;"><span class="glyphicon glyphicon-th"></span> online-TEST</div>
    </div>
    <div style="width: 40%; float: left;text-align: center;">
      <div style="color: #FFFFFF;font-size: 25px;font-weight: bold;font-style: italic;padding: 10px 15px;">Student's Page</div>
    </div>
    <div style="width: 30%; float: left;">
      <div style="text-align: right;color: #FFFFFF;font-size: 18px;padding: 15px 15px;"><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['nama'];?>&nbsp;&nbsp;&nbsp;<a href="logout.php" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a></div>
    </div>
  </div>
</nav>
