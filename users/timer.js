function counter(time){
  var interval = setInterval(function(){
    time = time - 1;
 	var jam=Math.floor(time/(60*60));
	var menit=Math.floor((time-jam*60*60)/60);
	var detik=time%60;
	dispwaktu=(jam<=9 ? "0" + jam : jam) + " : " + (menit<=9 ? "0" + menit : menit) + " : " + (detik<=9 ? "0" + detik : detik)+"";
    if(time <= 0){
      clearInterval(interval);
      endofTest();
    }
    $('#waktu').text(dispwaktu);
  }, 1000);
}
