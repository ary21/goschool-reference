  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h3 class="modal-title" id="myModalLabel"><font color="red">Konfirmasi</font></h3>
      </div>
      <div class="modal-body">  
       <p>Yakin akan mengakhiri tes ini?</p>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-primary" onclick="endofTest()">Yakin</button>
      </div>
    </form>
    </div>
  </div>
