<div class="article">
	<script type="text/javascript" src="js/timer.js"></script>
	<?php
		$waktu=600;
	?>
   <script type="text/javascript">
   		$(document).ready(function(){
  		var waktu = <?php echo $waktu;?>;
  		counter(waktu, 'index.php');
		});
   </script>
   <!--HEADER-->
	<div style="width:100%;margin-right: auto; margin-left: auto;">
		<div style="width:60%;float:left;"><h1 class="pageTitle">Tes Online:</h1></div>
		<div id="waktu" style="text-align:right; color:red; font-size:20px;width:40%; float:left;font-weight: bold">waktu</div>
	</div>

	<div style="width:100%;margin-right: auto; margin-left: auto;">
		<div style="width:60%;float:left;">
			<p style="font-size: 20px;font-weight: bold;">Soal nomor</p>
			<table>
				<tr><td colspan="2">Dari 10 orang siswa yang terdiri dari 7 orang putra dan 3 orang putri akan dibentuk tim yang beranggotakan 5 orang. Jika jumlah maksimal $\frac{1}{2}$ anggota putri dalam setiap tim adalah 2 orang, maka banyaknya tim yang dapat dibentuk adalah ....</td></tr>
				<tr><td width="50pt"><button type="button" id="pil_a" class="btn_pil">A</button></td>
					<td>
						$\int{\frac{\sqrt{x^2-2x}}{4x-4}dx}$
					</td>
				</tr>
				<tr><td width="50pt"><button type="button" id="pil_b" class="btn_pil_selected" disabled="">B</button></td>
					<td>
						$\frac{\frac{1}{2}}{4}$
					</td>
				</tr>
				<tr><td width="50pt"><button type="button" id="pil_c" class="btn_pil">C</button></td>
					<td>
						210
					</td>
				</tr>
				<tr><td width="50pt"><button type="button" id="pil_d" class="btn_pil">D</button></td><td>189</td></tr>
				<tr><td width="50pt"><button type="button" id="pil_e" class="btn_pil">E</button></td><td>168</td></tr>
			</table>
			<br>
			<table>
				<tr><td><button style="width:80px; height:40px;"><<</button></td><td></td><td></td><td></td></tr>
			</table>
		</div>
		<div style="width:10%;text-align:right; float:left;">
			&nbsp
		</div>
		<div style="width:30%;float:left;">
			<p style="font-size: 20px;font-weight: bold;">Pilih nomor</p>
			<table>
				<tr>
					<td><button type="button" id="no_001" class="btn_nomor_ragu">1</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">2</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">3</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">4</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">5</button></td>
				</tr>
				<tr>
					<td><button type="button" id="no_001" class="btn_nomor">6</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">7</button></td>
					<td><button type="button" id="no_001" class="btn_nomor_filled">8</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">9</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">10</button></td>
				</tr>
				<tr>
					<td><button type="button" id="no_001" class="btn_nomor">11</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">12</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">13</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">14</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">15</button></td>
				</tr>
				<tr>
					<td><button type="button" id="no_001" class="btn_nomor">16</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">17</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">18</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">19</button></td>
					<td><button type="button" id="no_001" class="btn_nomor">20</button></td>
				</tr>
			</table>
		</div>
	</div>
</div>