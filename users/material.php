<!-- Here's your modal begin -->
  <div class="modal-dialog" id="modal-dialog" style="width: 90%;height: 90%; overflow: hidden;margin-top: 0px">
    <?php
    $dat=$_REQUEST['dat'];
    $open=mysqli("UPDATE tb_learned SET open='1' WHERE  uid_material='".$dat."' AND uid_user='".$_SESSION['uid_user']."'");
    $learned=mysqli_fetch_assoc(mysqli("SELECT id_learned, uid_learned, open, upload, link_upload, time_open, status_learned FROM tb_learned WHERE uid_material='".$dat."' AND uid_user='".$_SESSION['uid_user']."'"));
    $uid_learned=$learned['uid_learned'];
    $status_learned=$learned['status_learned'];
    $upload=$learned['upload'];
    $link_upload=$learned['link_upload'];
    //

    $materi=mysqli_fetch_assoc(mysqli("
        SELECT t1.uid_user, t1.topic, t1.status, t1.reader, t2.nama_mapel, t1.topic, t1.material_content, t1.upload, t1.youtube, t1.link_youtube, t1.uid_mapel, t3.sure_name, t3.level
        FROM tb_material t1 
        JOIN tb_mapel t2 ON t1.uid_mapel=t2.uid_mapel
        JOIN tb_users t3 ON t1.uid_user=t3.uid_user
        WHERE t1.uid_material='".$dat."'"));
    //
    $uid_material=$dat;
    $uid_user=$_SESSION['uid_user'];
    $status_user=$_SESSION['level'];
    $sure_name=$_SESSION['nama'];
    ?>
    <div class="modal-content">
        <div class="modal-header" style="padding:5px;">
            <a href="main_page.php?view=page_material&dat=<?php echo $materi['uid_mapel'];?>" data-dismiss="modal" class="close" type="button" style="font-size: 36pt;color:black;">&times;</a>
            <h3 class="modal-title"><b><?php echo $materi['topic'];?></b></h3>
        </div>
        <div class="row">
            <div class="column left modal-body" style="font-family: sans-serif; overflow-x: hidden; font-size: 16px; overflow-y: auto;">
                <!--PETUNJUK-->
                <div class="alert" role="alert" style="background-color: #c4cfff;">
                    <b>Petunjuk Belajar:</b>
                    <ul>
                        <li>Baca dan pahami secara seksama materi pembelajaran yang diberikan.</li>
                        <li>Bila ada kesulitan, ayo kita budayakan untuk mendiskusikan di bagian tanya jawab.</li>
                        <li>Kerjakan soal/latihan/tugas yang diberikan.</li>
                        <li>Bila diperlukan mengirim file, gambar, dll silakan gunakan fasilitas upload file.</li>
                        <li>Berikan rate tingkat pemahaman agar Guru dapat memberikan materi pembelajaran lanjutan kepada Anda.</li>
                    </ul>
                </div>
                <!--MATERI PEMBELAJARAN-->
                <div class="alert" role="alert" style="background-color: #c4cfff;">
                    <b>Materi Pembelajaran</b>
                </div>
                <?php
                echo $materi['material_content'];
                if(strlen($materi['link_youtube'])!=0){
                    echo "<br>
                    <div class='embed-responsive embed-responsive-16by9'>
                        <iframe allowfullscreen='' class='embed-responsive-item' src=".str_replace("watch?v=","embed/",$materi['link_youtube']).">
                        </iframe>
                    </div>";
                }
                if($materi['upload']!=0){
                    echo "
                    <br>
                    <div class='alert' role='alert' style='background-color: #c4cfff;'>
                        <b>Upload Tugas (Max 1 Mb, type: png, jpg, doc, docx, xls, xlsx)</b>
                    </div>
                    <div class='col-lg-4'>
                        <div class='input-group'>
                            <input type='file' name='file_upload' id='file_upload' class='form-control'>
                            <span class='input-group-btn'>
                                <a href='#' onclick='upload()' class='btn btn-primary' type='button'>Upload</a>
                            </span>
                        </div>
                    </div>";
                }
                ?>
            </div>
            <div class="column right">
                <div style="background-color: green; color: white; font-size: 16pt; margin-bottom:10px; text-align: center">
                    <b>Tanya Jawab</b>
                </div>
                <div class="parent" id="messages-box" onscroll="myfunc()" style="font-size: 14px; overflow-y: auto; overflow-x: hidden; height:100%;">
                    <div class="direct-chat-messages" id="chats"></div>
                </div>
                <div>
                  <div class="input-group input-group-lg">
                    <textarea class="form-control" style="font-size: 14px;" id="comment" placeholder="Type message..." rows="3"></textarea>
                    <div class="input-group-btn">
                      <button onclick="kirim_comment()" type="button" class="btn" style="background-color: green; color: white">Submit</button>
                    </div>

                  </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="background-color: #EBEAEA;margin-bottom: 10px">
        <table width="100%">
            <tr>
                <td style="text-align: left;">Rate tingkat pemahaman: 
                    <a href="#1" id="1" onclick="rate(this.id)"><img src='../images/white_star.png'></a>
                    <a href="#2" id="2" onclick="rate(this.id)"><img src='../images/white_star.png'></a>
                    <a href="#3" id="3" onclick="rate(this.id)"><img src='../images/white_star.png'></a>
                    <a href="#4" id="4" onclick="rate(this.id)"><img src='../images/white_star.png'></a>
                    <a href="#5" id="5" onclick="rate(this.id)"><img src='../images/white_star.png'></a>
                </td>
            </tr>
        </table>       
        </div>     
    </div>
  </div>

<!-- End of Modal -->
<style type="text/css">
    .column {float: left; max-height: 400px;}
    .left {width: 70%; padding-left: 30px;}
    .right {width: 30%; padding-right: 20px;}
    .row:after {content: ""; display: table; clear: both;}
    .parent {display: flex; flex-direction: column; justify-content: space-between;}
    .direct-chat-messages{height:360px;}
    h4{color:red;}
</style>
<script type="text/javascript">
  var star="<?php echo $status_learned;?>";
  bintang(parseInt(star));
  //    
  function rate(id){
    var uid="<?php echo $uid_learned;?>";
    $.ajax({
        url: "../guru/fungsi.php",
        type: "POST",
        data: {funct:'rate', status_learned:id, uid_learned:uid},
        success: function(ajaxData){
            bintang(id);
        }
    });
  }
  //
  function bintang(id){
    var i;
    var arr=new Array("1","2","3","4","5");
    for (i=0; i<5; i++) {
        document.getElementById(arr[i]).innerHTML="<img src='../images/white_star.png'>";
    }
    for (i=0; i<parseInt(id); i++) {
        document.getElementById(arr[i]).innerHTML="<img src='../images/gold_star.png'>";
    }
  }
  //TANYA JAWAB
  var id_comment;
  var uid_material="<?php echo $uid_material;?>";
  var uid_user="<?php echo $uid_user;?>";
  var status_user="<?php echo $status_user;?>";
  var sure_name="<?php echo $sure_name;?>";
  MathJax.Hub.Queue(["Typeset",MathJax.Hub,'modal-dialog']);
  chat_history();
//
  function chat_history(){
    $.post('../guru/chat_history.php', {'uid_material': uid_material}, function(data) {
        $(data).hide().appendTo('.direct-chat-messages').fadeIn();
    });
    return false;
  }
//
  function kirim_comment(){
    var comment=$("#comment").val();
    if(comment!=""){
    $.post('../guru/chat_send.php', {'uid_material': uid_material, 'uid_user':uid_user, 'status_user': status_user, 'sure_name': sure_name, 'comment': comment}, function(data){
        $("#comment").val('');
        $("#comment").focus();
    });
    }
  }
//
    $('#comment').on('keypress', function(e) {
        if (e.keyCode === 13) {
            kirim_comment();
            $("#comment").val('');
            $("#comment").focus();
        }
    });
//
  function scroll_bar(n){
      $(".direct-chat-messages").scrollTop(n);    
  }
//
  setInterval(function(){
    chat_coming(id_comment);
  }, 1000);
//
  function chat_coming(id_comment){
    $.post('../guru/chat_coming.php', {'id_comment': id_comment, 'uid_material': uid_material}, function(data) {
      if(data.length!=0){
          $(data).hide().appendTo('.direct-chat-messages').fadeIn();
            var n=$(".direct-chat-messages")[0].scrollHeight;
      }
    });
    return false;
  }  

  //
  function upload(){
    if($('#file_upload').get(0).files.length!=0){
        var uid_learned="<?php echo $uid_learned;?>";
        var upload="<?php echo $upload;?>";
        var link_upload="<?php echo $link_upload;?>";
        var file_data = $('#file_upload').prop('files')[0];
        var form_data = new FormData();
        //                  
        form_data.append('file', file_data);
        $.ajax({
            url: '../users/upload.php?uid_learned='+uid_learned+'&upload='+upload+'&link_upload='+link_upload, //
            dataType: 'text',  //
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'POST',
            success: function(php_script_response){
                alert(php_script_response); //
            }
        });
    }else{
        alert("no file selected");
    }
  }
</script>
