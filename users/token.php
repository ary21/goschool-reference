<div class="col-xs-12">  
  <div class="box">
    <div class="box-body">
      <div align="center">
        <h2><b>Identitas Tes</b></h2>
        <br>
        <?php
          $q_idtes=mysqli_fetch_array(mysqli("SELECT t1.uid_tested, t3.nama_mapel, t2.nama_tes, t2.waktu, t2.token, t2.status_token FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t1.uid_tested='".$_REQUEST['uid_tested']."'"));
        ?>
        <table>
          <tr><td>Nama Tes</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $q_idtes['nama_tes'];?></td></tr>
          <tr><td>Mata Pelajaran</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $q_idtes['nama_mapel'];?></td></tr>
          <tr><td>Waktu yang tersedia</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $q_idtes['waktu'];?> menit</td></tr>
        </table>
        <br><br>
      <?php
        if($q_idtes['status_token']=="1"){
          echo "<p style='color:blue;'>Isilah token soal sesuai intruksi Pengawas/Proktor</p>
          <input type='text' id='tokenInput' style='font-size: 40px;text-align: center;' size='6' >
          <br>
          <p><span id='informasi'></span></p>
          ";
        }else{
          echo "<input type='hidden' id='tokenInput' value='".$q_idtes['token']."'>";
        }
        ?>
          <img src='../dist/img/ajax-loader-gif-6.gif' width='40px' id='ajax-loader'/>
          <br>
          <button class="btn btn-default" onclick="cancel()">Cancel</button>&nbsp;&nbsp;&nbsp;<button class="btn btn-primary" id="<?php echo $_REQUEST['uid_tested'];?>" onclick="submit_token(this.id)">Mulai Tes</button> 
        </form>
      </div>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div><!-- col-xs-12 -->
<script type="text/javascript">
  window.onload = function() {
    $("#ajax-loader").css("display", "none");
    $("#tokenInput").focus();
  }
  function cancel(){
    window.location='main_page.php?view=student_page';
  }

  function submit_token(id){
    $("#ajax-loader").css("display", "");
    var tokenInput=document.getElementById('tokenInput').value;
    $.ajax({
      url: "../users/check_token.php?tokenInput="+tokenInput+"&uid_tested="+id,
      type: "GET",
      dataType: 'html',
      success: function (ajaxData){
        if(ajaxData=="ok"){
          window.location="test_page.php?uid_tested="+id;
        }else{
          $("#informasi").html(ajaxData);
        }
      }
     })   
  }
</script>