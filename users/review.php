<?php
$a_tested=mysqli_fetch_array(mysqli("SELECT soal, shuffle_opsi, pilihan, kunci FROM tb_datatested WHERE uid_tested='".$dat."'"));
if($_SESSION['jenjang']=='sma'){
     $pilihan=array("kunci", "pengecoh_1", "pengecoh_2", "pengecoh_3", "pengecoh_4");
     $abcde=array("A","B","C","D","E");
}else{
     $pilihan=array("kunci", "pengecoh_1", "pengecoh_2", "pengecoh_3");
     $abcde=array("A","B","C","D");     
}
$opsi=explode(";", $a_tested['shuffle_opsi']);
$pil_tested=explode(";",$a_tested['pilihan']);
$kunci=explode(";",$a_tested['kunci']);
$soal=explode(";", $a_tested['soal']);
echo "<div style='width:60%; margin-left:200px;'>";
echo "<p align='right'><a href='main_page.php?view=student_page' class='btn btn-primary'><span class='glyphicon glyphicon-user'></span> Student Page</a></p>";
for($n=0;$n<count($soal);$n++){
     $a_soal=mysqli_fetch_array(mysqli("SELECT soal, kunci, pengecoh_1, pengecoh_2, pengecoh_3, pengecoh_4, sound, file_sound FROM tb_soal WHERE uid_soal='".$soal[$n]."'"));
     echo "
     <table>
     <tr><td rowspan='6' valign='top' width='60px' style='font-size: 24pt;color: red;'>".($n+1).".</td>
          <td colspan='2'>";
          if($a_soal['sound']=="ada"){
            echo "<audio controls><source src='".$a_soal['file_sound']."' type='audio/mpeg'>Your browser does not support the audio element.</audio><br>";
          }          
          echo $a_soal['soal']."</td></tr>";
          for ($i=0; $i < count($abcde); $i++) { 
               if($pil_tested[$n]==$abcde[$i]){
                    echo "<tr><td width='50px' valign='top'><button  id='".$abcde[$i]."' style='margin-bottom: 5pt;' class='btn btn-primary' onclick='jawaban(this.id)'>".$abcde[$i]."</button></td><td style='margin-bottom: 5pt;' >".$a_soal[$pilihan[substr($opsi[$n], $i, 1)]]."</td></tr>";
               }else{
                    echo "<tr><td width='50px' valign='top'><button  id='".$abcde[$i]."' style='margin-bottom: 5pt;' class='btn btn-default' onclick='jawaban(this.id)'>".$abcde[$i]."</button></td><td style='margin-bottom: 5pt;' >".$a_soal[$pilihan[substr($opsi[$n], $i, 1)]]."</td></tr>";
               }
          }
          echo "</table>";
          if($pil_tested[$n]==$kunci[$n]){
               echo "<p align='right'><IMG SRC='../dist/img/emoticon_happy.gif'></p>";
          }else{
               echo "<p align='right'><IMG SRC='../dist/img/emoticon_sad.gif'></p>";
          }
          echo "<hr>";
}
echo "</div>";
?>
