<div class="col-xs-12">  
  <div class="box">
     <div class="box-body">
        <script type="text/javascript">
            $(document).ready(function(){ 
            $("#myTab a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
                });
            });
        </script>
<p align="right"><button class="btn btn-primary" onclick="reload()"><span class="glyphicon glyphicon-refresh"></span>  Refresh</button></p>
<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#learning_material">Pembelajaran</a></li>
    <li><a href="#queue">Queue Tests</a></li>
    <li><a href="#result">Result</a></li>
    <li><a href="#review">Review</a></li>    
    <li><a href="#top20">Top 20</a></li>    
</ul> 

<div class="tab-content">
          <!--TAB 0-->
          <div id="learning_material" class="tab-pane fade in active">
            <?php
              $qlearned=mysqli("
                SELECT DISTINCT t3.nama_mapel, t3.uid_mapel FROM tb_learned t1
                JOIN tb_material t2 ON t1.uid_material=t2.uid_material
                JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel
                WHERE t1.uid_user='".$_SESSION['uid_user']."' AND t2.status='1'
                ORDER BY t3.id_mapel ASC");            
            ?>
            <br>
            <table class="table bordered">
              <thead>
                <tr><th width="30px">No.</th><th>Mata Pelajaran</th></tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                while($alearned=mysqli_fetch_assoc($qlearned)){
                  $q_n=mysqli("SELECT id_learned FROM tb_learned t1 JOIN tb_material t2 ON t1.uid_material=t2.uid_material WHERE t1.uid_user='".$_SESSION['uid_user']."' AND t2.uid_mapel='".$alearned['uid_mapel']."' AND t2.status='1'");
                  $n_material=mysqli_num_rows($q_n);
                  echo "<tr>
                  <td>".$no.".</td>
                  <td><a href='main_page.php?view=page_material&dat=".$alearned['uid_mapel']."'>".$alearned['nama_mapel']."<span class='badge'>".$n_material."</span></a>
                  </td>
                </tr>";
                $no=$no+1;
                }  
                ?>
              </tbody>
            </table>            
          </div>
          <!--TAB 1-->
          <div id="queue"  class="tab-pane fade">
            <?php
              $queue=mysqli("SELECT t1.uid_tested, t3.nama_mapel, t2.nama_tes, t2.waktu, t2.status_tes, t1.status FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t1.uid_user='".$_SESSION['uid_user']."' AND (t1.status='not yet' OR t1.status='on going') ORDER BY t2.update_tes DESC");
            ?>
            <br>
            <table class="table bordered">
              <thead>
                <tr><th>No.</th><th>Mapel</th><th>Nama Tes</th><th>Waktu</th><th>Status</th></tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                while($aqueue=mysqli_fetch_array($queue)){
                  echo "<tr><td>".$no.".</td><td>".$aqueue['nama_mapel']."</td><td>".$aqueue['nama_tes']."</td><td>".$aqueue['waktu']."</td>
                  <td>";
                  if($aqueue['status_tes']=="open"){
                    if($aqueue['status']=="not yet"){
                      echo "<a href='main_page.php?view=token&uid_tested=$aqueue[uid_tested]' class='btn btn-success'><span class='glyphicon glyphicon-file'></span> Start test</a>";  
                    }elseif ($aqueue['status']=="on going") {
                      echo "<a href='main_page.php?view=token&uid_tested=$aqueue[uid_tested]' class='btn btn-danger'><span class='glyphicon glyphicon-file'></span> ter-Logout</a>";  
                    }
                    
                  }else{
                    echo "<p class='btn btn-default''><span class='glyphicon glyphicon-ban-circle'></span>&nbsp;&nbsp;&nbsp;Close</p>";
                  }
                  echo "</td></tr>";
                  $no=$no+1;
                }
                
                ?>
              </tbody>
            </table>
          </div>

          <!--TAB 2-->         
          <div id="result"  class="tab-pane fade">
              <?php
              $result=mysqli("SELECT t1.uid_tested, t3.nama_mapel, t2.nama_tes, t2.hasil_tes, t2.retest, t1.start_time, t1.durasi,t1.nilai, t1.n_retest FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t1.uid_user='".$_SESSION['uid_user']."' AND t1.status='finish' ORDER BY t1.end_time DESC");
            ?>
            <br>
            <table class="table bordered">
              <thead>
                <tr><th>No.</th><th>Mapel</th><th>Nama Tes</th><th>Mulai</th><th>Durasi Kerja</th><th>Nilai</th><th></th></tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                while($aresult=mysqli_fetch_array($result)){
                  echo "<tr><td>".$no.".</td><td>".$aresult['nama_mapel']."</td><td>".$aresult['nama_tes']."</td><td>".$aresult['start_time']."</td><td>".$aresult['durasi']."</td>";
                  if($aresult['hasil_tes']=="0"){
                    echo "<td style='color:red;'>Nilai tidak diperlihatkan</td><td></td>";  
                  }else{
                    echo "<td style='font-size:24pt; color:blue;'><b>".$aresult['nilai']."</b></td>
                    <td>";
                    if($aresult['retest']=="1" && $aresult['n_retest']!="0"){
                      echo "<button id='".$aresult['uid_tested']."' class='btn btn-success' onclick='retest(this.id)'>Re-Test (".$aresult['n_retest'].")</button>";
                    }
                    echo "</td>";
                  }
                  echo "</tr>";
                  $no=$no+1;
                }
                
                ?>
              </tbody>
            </table>

          </div>

<!--TAB 3-->
          <div id="review"  class="tab-pane fade">
              <?php
              $result=mysqli("SELECT t1.uid_tested, t3.nama_mapel, t2.nama_tes, t2.uid_tes FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t1.uid_user='".$_SESSION['uid_user']."' AND t1.status='finish' AND t2.status_review='open' ORDER BY t1.end_time ASC");
            ?>
            <br>
            <table class="table bordered">
              <thead>
                <tr><th>No.</th><th>Mapel</th><th>Nama Tes</th><th>Review</th></tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                while($aresult=mysqli_fetch_array($result)){
                  echo "<tr><td>".$no.".</td><td>".$aresult['nama_mapel']."</td><td>".$aresult['nama_tes']."</td><td><a href='main_page.php?view=review&dat=".$aresult['uid_tested']."' class='btn btn-success'><span class='glyphicon glyphicon-search'></span> Review</a></td></tr>";
                  $no=$no+1;
                }
                
                ?>
              </tbody>
            </table>

          </div>

<!--TAB 4-->
          <div id="top20"  class="tab-pane fade">
              <?php
              $result=mysqli("SELECT t1.uid_tested, t3.nama_mapel, t2.nama_tes, t2.uid_tes FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t1.uid_user='".$_SESSION['uid_user']."' AND t1.status='finish' AND t2.status_top20='open' ORDER BY t1.end_time ASC");
            ?>
            <br>
            <table class="table bordered">
              <thead>
                <tr><th>No.</th><th>Mapel</th><th>Nama Tes</th><th>Top 20</th></tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                while($aresult=mysqli_fetch_array($result)){
                  echo "<tr><td>".$no.".</td><td>".$aresult['nama_mapel']."</td><td>".$aresult['nama_tes']."</td><td><a href='main_page.php?view=top20&dat=".$aresult['uid_tes']."' class='btn btn-success'><span class='glyphicon glyphicon-star'></span> Top 20</a></td></tr>";
                  $no=$no+1;
                }
                
                ?>
              </tbody>
            </table>

          </div>

</div> 
</div>
</div><!-- /.box-body -->
</div>
<script type="text/javascript">
  function reload(){
    window.location.reload();
    //var current_index = $("#myTab").tabs("option","selected");
    //$("#myTab").tabs('load',current_index);
  }
  function retest(id){
      $.ajax( {
          url: "../guru/fungsi.php?funct=reset_tested&uid_tested="+id,
          type: "GET",
          dataType: 'html',
          success: function (ajaxData){
            window.location.reload();
          }
     })   
  }

</script>