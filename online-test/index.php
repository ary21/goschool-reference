<!DOCTYPE html>
<html>
    <head>
        <title>Online-Test</title>
		<link rel="shortcut icon" href="../dist/img/favicon.png" />
		<link rel="stylesheet" type="text/css" href="login.css" />
    	<script type="text/javascript" src="../plugins/jQuery/jquery-1.12.3.min.js"></script>
		<script type="text/javascript" src="login.js"></script>
    </head>
<body style="font-family: sans-serif;">
<br><br>
<div style="width:80%;margin-right: auto; margin-left: auto;">
	<div style="width:60%; height:500px;float:left;-webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border-radius: 20px;
    background-color: #f1f1f1;
    color: #19468A;">
	<br>
	<IMG SRC="../dist/img/img_character.png" width="100%">
	<br><br>
	<p align="justify" style="line-height: 26px;margin-left: 40px;margin-right: 40px;font-size: 18px;">
		Aplikasi Tes Online ini merupakan sarana latihan soal, ulangan harian, remedial, dan try out secara online di lingkungan Yayasan Pupuk Kaltim.<br><br>
		Sebelum menggunakan aplikasi ini, Pengguna diharap login dengan username dan password yang telah diberikan oleh admin.<br><br>
		Setelah berhasil login, pengguna dapat melihat riwayat tes yang pernah dilakukan dan melihat daftar tes yang dapat dikerjakan.<br><br>
		Semoga aplikasi ini bermanfaat!
	</p>
	</div>
	<div style="width:10%; float:left;">&nbsp</div> 
	<div style="width:30%; float:left;">
		<div class="imgcontainer">
			<img src="../dist/img/img_avatar2.png" alt="Avatar" class="avatar">
		</div>
		<div class="container">
			<label><b>Username</b></label>
			<input type="text" placeholder="Enter Username" id="user_name" maxlength="20" required>
			<label><b>Password</b></label>
			<input type="password" placeholder="Enter Password" id="password" maxlength="10" required>
			<p align="center" id="pesan" style="color:red;"></p>
			<button type="submit" id="btn-login" onclick="check_login();" >Login</button>
			<input type="checkbox" checked="checked"> Remember me
		</div>

		<div class="container" style="background-color:#f1f1f1">
			<button type="button" class="cancelbtn"><a href="index.php">Cancel</a></button>
			<span class="psw">Forgot <a href="#">password?</a></span>
		</div>
	</div>
</div>
</body>
</html>