<?php 
  session_start();
  error_reporting(0);
  include "../config/koneksi.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>online-TEST</title>
    <link rel="shortcut icon" href="../dist/img/favicon.png" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <script type="text/javascript" src="../plugins/jQuery/jquery-1.12.3.min.js"></script>
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)'],['<span class="math-tex">\\(','\\)</span>']]}
      });
    </script>
    <script type="text/javascript" async src="../plugins/MathJax/MathJax.js?config=TeX-MML-AM_CHTML"></script>
  </head>

  <body style="font-size: 14pt;">
    <div class="wrapper">
      <header class="main-header">
          <?php include "../users/header.php"; ?>
      </header>

      <div class="content-wrapper" style="padding: 0px 25px";>
        <section class="content">
            <div class='row' id='preview'>
              <?php
                if(isset($_REQUEST['dat'])){
                  $dat=$_REQUEST['dat'];
                }
                $view="../users/".$_REQUEST['view'].".php";
                if (isset($_SESSION['uid_user'])) {
                  include $view;
                }else{
                  echo "<script>window.location='logout.php'</script>";                
                }
              ?>
            </div>
        </section>
      </div><!-- /.content-wrapper -->
    </div><!-- ./wrapper -->
    <div style="position:fixed;bottom:0; font-size: 10pt;background: black;width: 100%;text-align: center;color: white;">
      <?php include "footer.php"; ?>
    </div>
    
    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>

     <!--Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      window.history.pushState(null, "", window.location.href);        
      window.onpopstate = function() {
          window.history.pushState(null, "", window.location.href);
      };
  });
  </script>
  </body>
</html>