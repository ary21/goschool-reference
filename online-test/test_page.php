<?php 
  session_start();
  error_reporting(0);
  include "../config/koneksi.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>online-TEST</title>
    <link rel="shortcut icon" href="../dist/img/favicon.png" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <script type="text/javascript" src="../plugins/jQuery/jquery-1.12.3.min.js"></script>
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)'],['<span class="math-tex">\\(','\\)</span>']]}
      });
    </script>
    <script type="text/javascript" async src="../plugins/MathJax/MathJax.js?config=TeX-MML-AM_CHTML"></script>
    <script type="text/javascript">
      document.onkeydown = function(e) {
      if(event.keyCode == 123) {
          return false;
      }
      if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
            return false;
      }
      if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
            return false;
      }
      if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
            return false;
      }
      }
    </script>
    <style>
      .noselect {
          -webkit-touch-callout: none; /* iOS Safari */
          -webkit-user-select: none; /* Safari */
          -khtml-user-select: none; /* Konqueror HTML */
          -moz-user-select: none; /* Old versions of Firefox */
          -ms-user-select: none; /* Internet Explorer/Edge */
          user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome, Edge, Opera and Firefox */
      }
    </style>
  </head>

  <body style="font-size: 14pt;overflow-x: hidden;overflow-y: scroll;"  oncontextmenu="return false;">
    <div class="wrapper">
      <header class="main-header">
          <?php 
          include "../users/test_header.php"; 
          $uid_tested=$_REQUEST['uid_tested'];
            $datatested=mysqli_fetch_array(mysqli("SELECT t1.uid_tes, t1.start_time, t1.to_time, t1.soal, t1.status, t2.jenjang, t2.waktu, t2.nama_tes, t3.nama_mapel FROM tb_datatested t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE uid_tested='".$_REQUEST['uid_tested']."'"));
            $waktu_off=$datatested['to_time'];
            $status=$datatested['status'];
            $soal=explode(";", $datatested['soal']);
            $max_soal=count($soal);
            $jenjang=$datatested['jenjang'];
          ?>
      </header>
      <div class="content-wrapper" style="padding: 0px 50px;">
        <section class="content">
          <div style="width: 100%; overflow: hidden;">
              <div style="width: 70%; float: left;">
                <table width="100%"><tr><td align="left" width="50%" style="border-bottom:1pt solid black;"><b><h3><?php echo $datatested['nama_mapel']."-".$datatested['nama_tes'];?></h3></b></td><td valign="middle" style="border-bottom:1pt solid black;"><img src="../dist/img/ajax-loader-gif-6.gif" width="40px" id="ajax-loader"/></td>
                  <td align="right" width="40%" style="border-bottom:1pt solid black;">
                    <span id="waktu" style="text-align:right; color:red; font-size:24px;font-weight: bold;">waktu kerja</span>&nbsp;&nbsp;
                    <button class="btn btn-primary" id="prev_butt" onclick="decrease()"><span class="glyphicon glyphicon-arrow-left"></span></button>&nbsp;&nbsp;
                    <button class="btn btn-primary" id="next_butt" onclick="increase(this.id)"><span class="glyphicon glyphicon-arrow-right"></span></button></td></tr></table>
                <br>
                <div id='preview' class="noselect"></div>
                <hr>
              </div>

              <div style="width: 10%; float: left;">&nbsp;</div>

              <div style="width: 20%; float: left;">
                <h3><font color="#367FA9">Navigasi Soal</font></h3>
                <br>
                <div style="background-color: #E8E8FF; padding: 10pt;border-radius: 15px;">
                <table border="0" cellpadding="5px" cellspacing="5px" width="100%">
                  <?php
                    $row=1;
                    for($i=1;$i<=$max_soal;$i++){
                      if($row==1){
                        echo "<tr>";
                      }
                      if($row<=5){
                        echo "<td><button class='btn btn-default' style='width:30pt;margin-bottom: 8pt;' id='".$i."' onclick='increase(this.id)'>".$i."</button></td>";
                      }
                      if($row==5){
                        echo "</tr>";
                        $row=0;
                      }
                      $row=$row+1;
                    }
                  ?>
                </table>

                <br><br>
              <p><button class="btn btn-danger" onclick="modal_endtest()"><span class="glyphicon glyphicon-off"></span> Mengakhiri Tes</button></p>
              </div>
              </div>
            </div>
        </section>
      </div><!-- /.content-wrapper -->
      <!--/MODAL -->
      <div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
    </div><!-- ./wrapper -->
    <div style="position:fixed;bottom:0; font-size: 10pt;background: black;width: 100%;text-align: center;color: white;">
      <?php include "footer.php"; ?>
    </div>

<script type="text/javascript">
  var n_soal=1;
  var max_soal='<?php echo $max_soal;?>';
  var uid_tested = '<?php echo $uid_tested;?>';
  var jenjang = '<?php echo $jenjang;?>';
  var waktu_off='<?php echo $waktu_off;?>';
  var status_tes='<?php echo $status;?>';
  var end="end="+waktu_off;
  //
  $(document).ready(function() {
      window.history.pushState(null, "", window.location.href);        
      window.onpopstate = function() {
          window.history.pushState(null, "", window.location.href);
      };
  });
  //
  window.onload = function() {
    document.getElementById("prev_butt").disabled = true;
    soal_change(n_soal);
    check_navi();
  }
  //
  var interval = setInterval(function(){
    $.get("timer.php", end, function(result){
      if(status_tes!="finish"){
        if(result>0){
          disp_timer(result);
        }else{
          clearInterval(interval);
          endofTest();
        }
      }else{
        clearInterval(interval);
        endofTest();
      }
    });
  }, 1000);
//
  function disp_timer(data){
    var jam=Math.floor(data/(60*60));
      var menit=Math.floor((data-jam*60*60)/60);
      var detik=data%60;
      dispwaktu=(jam<=9 ? "0" + jam : jam) + " : " + (menit<=9 ? "0" + menit : menit) + " : " + (detik<=9 ? "0" + detik : detik)+"";
      $("#waktu").html(dispwaktu);
  }
//
  function decrease() {
    if(n_soal==1){
      n_soal=1;
    }else{
      n_soal=n_soal-1;
      document.getElementById("prev_butt").disabled = false;
      document.getElementById("next_butt").disabled = false;
      if(n_soal==1){
        document.getElementById("prev_butt").disabled = true;
      }
      soal_change(n_soal);
    }
    
  }
  function increase(data){
    if(data=="next_butt"){
      n_soal=n_soal;
    }else{
      n_soal=Number(data)-1;
    }
    if(n_soal==max_soal){
      n_soal=max_soal;
    }else{
      n_soal=n_soal+1;
      document.getElementById("prev_butt").disabled = false;
      document.getElementById("next_butt").disabled = false;
      if(n_soal==max_soal){
        document.getElementById("next_butt").disabled = true;
      }
      soal_change(n_soal);
    }
    
  }

  function soal_change(n_soal){
    $("#ajax-loader").css("display", "");
    $.ajax({
      url: "../users/soal.php?uid_tested="+uid_tested+"&nomor="+n_soal,
      type: "GET",
      dataType: 'html',
      success: function (ajaxData){
        if(ajaxData!="finish"){
          $("#preview").html(ajaxData);
          var n_soal=n_soal;
          $("#ajax-loader").css("display", "none");
          MathJax.Hub.Queue(["Typeset",MathJax.Hub,'preview']);
        }else{
          endofTest();
        }
      }
     })   
  }

  function jawaban(id){
    if(jenjang=='sma'){
     document.getElementById("A").className = 'btn btn-default';
     document.getElementById("B").className = 'btn btn-default';
     document.getElementById("C").className = 'btn btn-default';
     document.getElementById("D").className = 'btn btn-default';
     document.getElementById("E").className = 'btn btn-default';
    }else{
     document.getElementById("A").className = 'btn btn-default';
     document.getElementById("B").className = 'btn btn-default';
     document.getElementById("C").className = 'btn btn-default';
     document.getElementById("D").className = 'btn btn-default';
    }
     $("#ajax-loader").css("display", "");
     $.ajax({
      url:"../users/klik_pilihan.php?uid_tested="+uid_tested+"&nomor="+n_soal+"&pilih="+id,
      type:"GET",
      dataType: 'html',
      success: function(ajaxData){
        $("#ajax-loader").css("display", "none");
        if(ajaxData=="ok"){
          document.getElementById(id).className = 'btn btn-primary';
          document.getElementById(n_soal).className = 'btn btn-primary';
        }
      }
     })
  }
  
  function ragu(id){
    $.ajax({
      url:"../users/klik_ragu.php?uid_tested="+uid_tested+"&nomor="+n_soal,
      type:"GET",
      dataType: 'html',
      success: function(ajaxData){
        if(ajaxData=="1"){
          document.getElementById(id).className = 'btn btn-danger';
          document.getElementById(n_soal).className = 'btn btn-danger';
        }else if(ajaxData=="0"){
          document.getElementById(id).className = 'btn btn-default';
          document.getElementById(n_soal).className = 'btn btn-primary';
        }
      }
    })
  }

  function check_navi(){
    $.ajax({
      url:"../users/check_navi.php?uid_tested="+uid_tested,
      type: 'POST',
      dataType:"html",
      success: function(ajaxData){
        var obj = $.parseJSON(ajaxData);
         for (var i = 1; i <= max_soal; i++) {
            if(obj[i-1] !== "*"){
              navi_on(i);
            }
          }
          check_ragu();
        }   
    })
  }

  function check_ragu(){
    $.ajax({
      url:"../users/check_ragu.php?uid_tested="+uid_tested,
      type: 'POST',
      dataType:"html",
      success: function(ajaxData){
        var obj = $.parseJSON(ajaxData);
         for (var i = 1; i <= max_soal; i++) {
            if(obj[i-1] == "1"){
              navi_ragu(i);
            }
          }
        }   
    })
  }

  function navi_on(nav){
    document.getElementById(nav).className = 'btn btn-primary';
  }

  function navi_ragu(nav){
    document.getElementById(nav).className = 'btn btn-danger';
  }
  function modal_endtest(){
    $.ajax({
      url: "../users/modal_endtest.php",
      success: function (ajaxData){
        $("#ModalUtama").html(ajaxData);
        $("#ModalUtama").modal('show',{backdrop: 'true'});
      }
    });
  }

   function endofTest(){
    $.ajax({
      url:"../users/periksa_hasil.php?uid_tested="+uid_tested,
      type: 'POST',
      dataType:"html",
      success: function(ajaxData){
        if(ajaxData=="ok"){
          window.location="main_page.php?view=student_page";
        }
      }
    })
  }
</script>
    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>

     <!--Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>