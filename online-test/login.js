function check_login(){
	var username = $('#user_name').val();
	var password = $('#password').val();
	var url_login	 = '../users/login_process.php';
	var url_user	 = 'main_page.php?view=student_page';
	$('#pesan').html('Silahkan tunggu ...');
	
	//Gunakan jquery AJAX
	$.ajax({
		url		: url_login,
		data	: 'user_name='+username+'&password='+password, 
		type	: 'POST',
		dataType: 'html',
		success	: function(pesan){
			if(pesan=='ok'){
				window.location = url_user;
			}
			else{
				$('#pesan').html(pesan);
			}
		},
	});
}
