<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIABS | Log in</title>
    <link rel="shortcut icon" href="dist/img/favicon.png" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  </head>
  <body class="hold-transition login-page">

    <div class="login-box">
      <div class="login-logo">
        <a href="#"><b>SiABS</b> Sekolah</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Silakan login pada form di bawah ini</p>

        <form action="" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name='a' placeholder="Username"  maxlength="10" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name='b' placeholder="Password" maxlength="10" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button name='login' type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
<?php 
 include_once "config/koneksi.php";
if (isset($_POST['login'])){
	if($_POST['a']==="admin"){
		 $pass=md5(base64_encode(htmlspecialchars($_POST['b'])));
	}else{
		$pass=htmlspecialchars($_POST['b']);
	}
 $user = mysqli("SELECT * FROM tb_users WHERE _username='".$_POST['a']."' AND _password='".$pass."' AND level<>'siswa'");
 
 $hitunguser = mysqli_num_rows($user);
 if ($hitunguser >0){
    $r = mysqli_fetch_array($user);
    $_SESSION['id_user']     = $r['uid_user'];
    $_SESSION['surename']  = $r['sure_name'];
    $_SESSION['level']    = $r['level'];
    include "config/user_agent.php";
    mysqli("INSERT INTO tb_users_aktivitas VALUES('','".$r['sure_name']."','$ip','$user_browser $version','$user_os','".$r['level']."','".date('H:i:s')."','".date('Y-m-d')."')");
    echo "<script>document.location='view/preview.php?view=home';</script>";
 }else{
    echo "<script>window.alert('Maaf, Anda Tidak Memiliki akses');
                  window.location=('index_guru.php')</script>";
 }
}
?>