<section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $foto; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['surename']; ?></p>
              <a href="#"><i class="glyphicon glyphicon-home"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header" style='color:#fff; text-transform:uppercase; border-bottom:2px solid #00c0ef'>MENU <?php echo $level; ?></li>
            <li><a href="preview.php?view=home"><i class=" glyphicon glyphicon-dashboard"></i><span>Dashboard</span></a></li>
            
            <li><a href="preview.php?view=mapel_admin"><i class=" glyphicon glyphicon-book"></i><span>Pengelolaan Mapel</span></a></li>
            <li><a href="preview.php?view=guru_admin"><i class="glyphicon glyphicon-user"></i><span>Pengelolaan Guru </span></a></li>
            <li class="treeview">
              <a href="preview.php?view=pd_admin"><i class="glyphicon glyphicon-th-large"></i><span>Pengelolaan PD</span></a>
            </li>

            <li><a href="preview.php?view=tes_setting"><i class="glyphicon glyphicon-check"></i><span>Pengelolaan Tes</span></a></li>
            <li><a href="preview.php?view=peserta_tes"><i class="glyphicon glyphicon-th-list"></i><span>Peserta Tes</span></a></li>
            <li><a href="#"><i class="glyphicon glyphicon-cog"></i></i> <span>Setting</span></a></li>
          </ul>
        </section>