<section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $foto; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header" style='color:#fff; text-transform:uppercase; border-bottom:2px solid #00c0ef'>MENU <?php echo $level; ?></li>
            <li><a href="preview.php?view=home"><i class="glyphicon glyphicon-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="preview.php?view=material_page"><i class="glyphicon glyphicon-book"></i><span> Pembuatan Materi OL</span></a></li>
            <li><a href="preview.php?view=peserta_belajar"><i class="glyphicon glyphicon-user"></i><span> Peserta Belajar</span></a></li>
            <li><a href="preview.php?view=pokok_bahasan"><i class="glyphicon glyphicon-duplicate"></i><span> Pembuatan Soal</span></a></li>
            <li><a href="preview.php?view=tes_setting"><i class="glyphicon glyphicon-file"></i><span>Ulangan/Tes</span></a></li>
            <li><a href="preview.php?view=peserta_tes"><i class="glyphicon glyphicon-user"></i> <span>Peserta Tes</span></a></li>
        </section>