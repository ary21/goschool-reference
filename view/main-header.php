<style type="text/css">
  .sekolah{
    float: left;
    background-color: transparent;
    background-image: none;
    padding: 15px 16px;
    font-family: fontAwesome;
    color:#fff;
  }

  .sekolah:hover{
    color:#fff;
  }
</style>
        <!-- Logo -->
        <a href="index.php" class="logo" style="text-align: left;">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="glyphicon glyphicon-th"></span><span class="logo-lg"><b> SiABS Sekolah</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <span class="textlogo"><b> Sistem Aplikasi Bank Soal</b></span>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo $foto; ?>" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $_SESSION['surename']; ?></span> <span class='caret'></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo $foto; ?>" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $_SESSION['surename']; ?>
                      <small><?php echo $_SESSION['level']; ?></small>
                    </p>
                  </li>
                  <!-- Menu Body --
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>
                  -- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="index.php" class="btn btn-default btn-flat">Dashboard</a>
                    </div>
                    <div class="pull-right">
                      <?php 
                        if ($_SESSION[level]=='admin'){
                            echo "<a href='index.php?view=admin&act=edit&id=$_SESSION[id]' class='btn btn-default btn-flat'>Edit Profile</a>";
                        }elseif($_SESSION[level]=='guru'){
                            echo "<a href='index.php?view=admin&act=edit&id=$_SESSION[id]' class='btn btn-default btn-flat'>Edit Profile</a>";    
                        }elseif($_SESSION[level]=='siswa'){
                            echo "<a href='index.php?view=siswa&act=detailsiswa&id=$_SESSION[id]' class='btn btn-default btn-flat'>View Profile</a>";
                        }
                      ?>
                    </div>
                  </li>
                </ul>
              </li>
              <li><a href="logout.php" >Logout</a></li>
            </ul>
          </div>
        </nav>