-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2020 at 03:17 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_siabs_source`
--

-- --------------------------------------------------------

--
-- Table structure for table `hasil_peminatan_back`
--

CREATE TABLE `hasil_peminatan_back` (
  `id_hasil` int(11) NOT NULL,
  `uid_user` varchar(20) NOT NULL,
  `Nama_siswa` varchar(100) NOT NULL,
  `mat` varchar(10) NOT NULL,
  `ipa` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tb_comment`
--

CREATE TABLE `tb_comment` (
  `id_comment` int(11) NOT NULL,
  `uid_comment` varchar(10) DEFAULT NULL,
  `uid_material` varchar(10) DEFAULT NULL,
  `uid_user` varchar(10) DEFAULT NULL,
  `comment` text,
  `status_user` varchar(10) DEFAULT NULL,
  `update_comment` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_comment`
--
DELIMITER $$
CREATE TRIGGER `uid_comment` BEFORE INSERT ON `tb_comment` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_comment IS NULL OR NEW.uid_comment = '')
 THEN SET NEW.uid_comment =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_datakelas`
--

CREATE TABLE `tb_datakelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_datatested`
--

CREATE TABLE `tb_datatested` (
  `id_datatested` int(11) NOT NULL,
  `uid_tested` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `uid_tes` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `soal` longtext,
  `shuffle_opsi` longtext,
  `pilihan` longtext,
  `kunci` longtext,
  `status_ragu` text,
  `nilai` varchar(6) DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `to_time` varchar(50) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL,
  `durasi` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'not yet',
  `n_retest` varchar(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_datatested`
--
DELIMITER $$
CREATE TRIGGER `uid_tested` BEFORE INSERT ON `tb_datatested` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_tested IS NULL OR NEW.uid_tested = '')
 THEN SET NEW.uid_tested =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_datatested_back`
--

CREATE TABLE `tb_datatested_back` (
  `id_datatested` int(11) NOT NULL,
  `uid_tested` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_tes` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `soal` longtext NOT NULL,
  `shuffle_opsi` text NOT NULL,
  `pilihan` longtext NOT NULL,
  `status_ragu` text NOT NULL,
  `nilai` varchar(6) NOT NULL,
  `start_time` varchar(50) NOT NULL,
  `end_time` varchar(50) NOT NULL,
  `durasi` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'not yet'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_datatested_to2019`
--

CREATE TABLE `tb_datatested_to2019` (
  `id_datatested` int(11) NOT NULL,
  `uid_tested` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_tes` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `soal` longtext NOT NULL,
  `shuffle_opsi` text NOT NULL,
  `pilihan` longtext NOT NULL,
  `status_ragu` text NOT NULL,
  `nilai` varchar(6) NOT NULL,
  `start_time` varchar(50) NOT NULL,
  `end_time` varchar(50) NOT NULL,
  `durasi` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'not yet'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru_mapel`
--

CREATE TABLE `tb_guru_mapel` (
  `id_gurumapel` int(11) NOT NULL,
  `uid_gurumapel` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_mapel` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_guru_mapel`
--
DELIMITER $$
CREATE TRIGGER `uid_gurumapel` BEFORE INSERT ON `tb_guru_mapel` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(5);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=5 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_gurumapel IS NULL OR NEW.uid_gurumapel = '')
 THEN SET NEW.uid_gurumapel =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_imagesoal`
--

CREATE TABLE `tb_imagesoal` (
  `id_imagesoal` int(11) NOT NULL,
  `nama_image` varchar(100) NOT NULL,
  `update_image` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_learned`
--

CREATE TABLE `tb_learned` (
  `id_learned` int(11) NOT NULL,
  `uid_learned` varchar(10) DEFAULT NULL,
  `uid_user` varchar(10) DEFAULT NULL,
  `uid_material` varchar(10) DEFAULT NULL,
  `open` int(1) NOT NULL DEFAULT '0',
  `upload` int(1) NOT NULL DEFAULT '0',
  `link_upload` varchar(50) DEFAULT NULL,
  `time_open` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_learned` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_learned`
--
DELIMITER $$
CREATE TRIGGER `uid_learned` BEFORE INSERT ON `tb_learned` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_learned IS NULL OR NEW.uid_learned = '')
 THEN SET NEW.uid_learned =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_mapel`
--

CREATE TABLE `tb_mapel` (
  `id_mapel` int(11) NOT NULL,
  `uid_mapel` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `nama_mapel` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_mapel`
--
DELIMITER $$
CREATE TRIGGER `uid_mapel` BEFORE INSERT ON `tb_mapel` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(5);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=5 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_mapel IS NULL OR NEW.uid_mapel = '')
 THEN SET NEW.uid_mapel =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_material`
--

CREATE TABLE `tb_material` (
  `id_material` int(11) NOT NULL,
  `uid_material` varchar(10) DEFAULT NULL,
  `uid_mapel` varchar(20) DEFAULT NULL,
  `uid_user` varchar(10) DEFAULT NULL,
  `topic` varchar(50) DEFAULT NULL,
  `material_content` longtext,
  `upload` int(1) NOT NULL DEFAULT '0',
  `youtube` varchar(1) NOT NULL DEFAULT '0',
  `link_youtube` text,
  `status` int(1) NOT NULL DEFAULT '0',
  `reader` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_material`
--
DELIMITER $$
CREATE TRIGGER `uid_material` BEFORE INSERT ON `tb_material` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_material IS NULL OR NEW.uid_material = '')
 THEN SET NEW.uid_material =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pb`
--

CREATE TABLE `tb_pb` (
  `id_pb` int(11) NOT NULL,
  `uid_pb` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `jenjang` enum('sd','smp','sma') NOT NULL DEFAULT 'sma',
  `uid_mapel` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `pb` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_pb`
--
DELIMITER $$
CREATE TRIGGER `uid_pb` BEFORE INSERT ON `tb_pb` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(5);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=5 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_pb IS NULL OR NEW.uid_pb = '')
 THEN SET NEW.uid_pb =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_rombel`
--

CREATE TABLE `tb_rombel` (
  `id_rombel` int(11) NOT NULL,
  `uid_pd` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `id_kelas` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_rombel`
--
DELIMITER $$
CREATE TRIGGER `uid_pd` BEFORE INSERT ON `tb_rombel` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_pd IS NULL OR NEW.uid_pd = '')
 THEN SET NEW.uid_pd =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_soal`
--

CREATE TABLE `tb_soal` (
  `id_soal` int(11) NOT NULL,
  `uid_soal` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_mapel` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_pb` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `video` varchar(50) NOT NULL DEFAULT 'tidak ada',
  `sound` varchar(50) NOT NULL DEFAULT 'tidak ada',
  `soal` text NOT NULL,
  `kunci` text NOT NULL,
  `pengecoh_1` text NOT NULL,
  `pengecoh_2` text NOT NULL,
  `pengecoh_3` text NOT NULL,
  `pengecoh_4` text NOT NULL,
  `file_sound` varchar(100) NOT NULL,
  `update_soal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_soal`
--
DELIMITER $$
CREATE TRIGGER `uid_soal` BEFORE INSERT ON `tb_soal` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_soal IS NULL OR NEW.uid_soal = '')
 THEN SET NEW.uid_soal =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_soalrand`
--

CREATE TABLE `tb_soalrand` (
  `id_soalrand` int(11) NOT NULL,
  `uid_soalrand` varchar(10) NOT NULL,
  `uid_tes` varchar(10) NOT NULL,
  `uid_pb` varchar(10) NOT NULL,
  `jml_soal` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_soalrand`
--
DELIMITER $$
CREATE TRIGGER `uid_soalrand` BEFORE INSERT ON `tb_soalrand` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_soalrand IS NULL OR NEW.uid_soalrand = '')
 THEN SET NEW.uid_soalrand =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_soaltes`
--

CREATE TABLE `tb_soaltes` (
  `id_soaltes` int(11) NOT NULL,
  `uid_soaltes` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_tes` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `uid_soal` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `layout_soal` varchar(1) NOT NULL DEFAULT '1',
  `n_opsi` varchar(1) NOT NULL DEFAULT '5',
  `no_soal` varchar(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Triggers `tb_soaltes`
--
DELIMITER $$
CREATE TRIGGER `uid_soaltes` BEFORE INSERT ON `tb_soaltes` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_soaltes IS NULL OR NEW.uid_soaltes = '')
 THEN SET NEW.uid_soaltes =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tes`
--

CREATE TABLE `tb_tes` (
  `id_tes` int(11) NOT NULL,
  `uid_tes` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `jenjang` enum('sd','smp','sma') NOT NULL DEFAULT 'sma',
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `uid_mapel` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `nama_tes` varchar(100) DEFAULT NULL,
  `waktu` varchar(5) NOT NULL DEFAULT '60',
  `start_tes` varchar(20) DEFAULT NULL,
  `end_tes` varchar(20) DEFAULT NULL,
  `mode_ambil` varchar(20) NOT NULL DEFAULT 'manual',
  `mode_tampil` varchar(10) NOT NULL DEFAULT 'random',
  `status_tes` varchar(10) NOT NULL DEFAULT 'close',
  `token` varchar(6) DEFAULT NULL,
  `status_token` varchar(1) NOT NULL DEFAULT '1',
  `status_review` enum('close','open') NOT NULL DEFAULT 'close',
  `status_top20` enum('close','open') NOT NULL DEFAULT 'close',
  `hasil_tes` varchar(1) NOT NULL DEFAULT '1',
  `retest` varchar(1) NOT NULL DEFAULT '0',
  `n_retest` varchar(1) NOT NULL DEFAULT '2',
  `update_tes` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_tes`
--
DELIMITER $$
CREATE TRIGGER `uid_tes` BEFORE INSERT ON `tb_tes` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_tes IS NULL OR NEW.uid_tes = '')
 THEN SET NEW.uid_tes =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `id_user` int(11) NOT NULL,
  `uid_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `jenjang` enum('sd','smp','sma') NOT NULL DEFAULT 'sma',
  `_username` varchar(50) NOT NULL,
  `_password` varchar(500) NOT NULL,
  `sure_name` varchar(50) NOT NULL,
  `id_number` varchar(20) NOT NULL,
  `level` enum('siswa','guru','admin') NOT NULL,
  `status` enum('aktif','tidak') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Triggers `tb_users`
--
DELIMITER $$
CREATE TRIGGER `uid_user` BEFORE INSERT ON `tb_users` FOR EACH ROW BEGIN
DECLARE HURUF CHAR(62);
DECLARE i INT;
DECLARE UID CHAR(10);
SET HURUF="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
SET UID="";
SET i=1;
WHILE i<=10 do
SET UID=CONCAT(UID,SUBSTR(HURUF,FLOOR(RAND()*62),1));
SET i=i+1; 
END WHILE;
IF(NEW.uid_user IS NULL OR NEW.uid_user = '')
 THEN SET NEW.uid_user =UID;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_users_aktivitas`
--

CREATE TABLE `tb_users_aktivitas` (
  `id_users_aktivitas` int(10) NOT NULL,
  `identitas` varchar(50) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `browser` varchar(50) NOT NULL,
  `os` varchar(50) NOT NULL,
  `status` enum('siswa','guru','admin') NOT NULL,
  `jam` time NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hasil_peminatan_back`
--
ALTER TABLE `hasil_peminatan_back`
  ADD PRIMARY KEY (`id_hasil`);

--
-- Indexes for table `tb_comment`
--
ALTER TABLE `tb_comment`
  ADD PRIMARY KEY (`id_comment`);

--
-- Indexes for table `tb_datakelas`
--
ALTER TABLE `tb_datakelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tb_datatested`
--
ALTER TABLE `tb_datatested`
  ADD PRIMARY KEY (`id_datatested`),
  ADD KEY `uid_tested_2` (`uid_tested`),
  ADD KEY `uid_tes` (`uid_tes`),
  ADD KEY `uid_user` (`uid_user`);
ALTER TABLE `tb_datatested` ADD FULLTEXT KEY `uid_tested` (`uid_tested`);

--
-- Indexes for table `tb_datatested_back`
--
ALTER TABLE `tb_datatested_back`
  ADD PRIMARY KEY (`id_datatested`);

--
-- Indexes for table `tb_datatested_to2019`
--
ALTER TABLE `tb_datatested_to2019`
  ADD PRIMARY KEY (`id_datatested`);

--
-- Indexes for table `tb_guru_mapel`
--
ALTER TABLE `tb_guru_mapel`
  ADD PRIMARY KEY (`id_gurumapel`);

--
-- Indexes for table `tb_imagesoal`
--
ALTER TABLE `tb_imagesoal`
  ADD PRIMARY KEY (`id_imagesoal`);

--
-- Indexes for table `tb_learned`
--
ALTER TABLE `tb_learned`
  ADD PRIMARY KEY (`id_learned`);

--
-- Indexes for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tb_material`
--
ALTER TABLE `tb_material`
  ADD PRIMARY KEY (`id_material`);

--
-- Indexes for table `tb_pb`
--
ALTER TABLE `tb_pb`
  ADD PRIMARY KEY (`id_pb`);

--
-- Indexes for table `tb_rombel`
--
ALTER TABLE `tb_rombel`
  ADD PRIMARY KEY (`id_rombel`);

--
-- Indexes for table `tb_soal`
--
ALTER TABLE `tb_soal`
  ADD PRIMARY KEY (`id_soal`),
  ADD KEY `uid_soal` (`uid_soal`),
  ADD KEY `uid_mapel` (`uid_mapel`),
  ADD KEY `uid_pb` (`uid_pb`);

--
-- Indexes for table `tb_soalrand`
--
ALTER TABLE `tb_soalrand`
  ADD PRIMARY KEY (`id_soalrand`);

--
-- Indexes for table `tb_soaltes`
--
ALTER TABLE `tb_soaltes`
  ADD PRIMARY KEY (`id_soaltes`),
  ADD KEY `uid_tes` (`uid_tes`);

--
-- Indexes for table `tb_tes`
--
ALTER TABLE `tb_tes`
  ADD PRIMARY KEY (`id_tes`),
  ADD KEY `uid_tes` (`uid_tes`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `uid_user` (`uid_user`);

--
-- Indexes for table `tb_users_aktivitas`
--
ALTER TABLE `tb_users_aktivitas`
  ADD PRIMARY KEY (`id_users_aktivitas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hasil_peminatan_back`
--
ALTER TABLE `hasil_peminatan_back`
  MODIFY `id_hasil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT for table `tb_comment`
--
ALTER TABLE `tb_comment`
  MODIFY `id_comment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_datakelas`
--
ALTER TABLE `tb_datakelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tb_datatested`
--
ALTER TABLE `tb_datatested`
  MODIFY `id_datatested` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10917;
--
-- AUTO_INCREMENT for table `tb_datatested_back`
--
ALTER TABLE `tb_datatested_back`
  MODIFY `id_datatested` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2474;
--
-- AUTO_INCREMENT for table `tb_datatested_to2019`
--
ALTER TABLE `tb_datatested_to2019`
  MODIFY `id_datatested` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10820;
--
-- AUTO_INCREMENT for table `tb_guru_mapel`
--
ALTER TABLE `tb_guru_mapel`
  MODIFY `id_gurumapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `tb_imagesoal`
--
ALTER TABLE `tb_imagesoal`
  MODIFY `id_imagesoal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1758;
--
-- AUTO_INCREMENT for table `tb_learned`
--
ALTER TABLE `tb_learned`
  MODIFY `id_learned` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `tb_material`
--
ALTER TABLE `tb_material`
  MODIFY `id_material` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_pb`
--
ALTER TABLE `tb_pb`
  MODIFY `id_pb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=498;
--
-- AUTO_INCREMENT for table `tb_rombel`
--
ALTER TABLE `tb_rombel`
  MODIFY `id_rombel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=535;
--
-- AUTO_INCREMENT for table `tb_soal`
--
ALTER TABLE `tb_soal`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6999;
--
-- AUTO_INCREMENT for table `tb_soalrand`
--
ALTER TABLE `tb_soalrand`
  MODIFY `id_soalrand` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_soaltes`
--
ALTER TABLE `tb_soaltes`
  MODIFY `id_soaltes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8587;
--
-- AUTO_INCREMENT for table `tb_tes`
--
ALTER TABLE `tb_tes`
  MODIFY `id_tes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;
--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1217;
--
-- AUTO_INCREMENT for table `tb_users_aktivitas`
--
ALTER TABLE `tb_users_aktivitas`
  MODIFY `id_users_aktivitas` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1220;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
