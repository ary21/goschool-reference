  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Menambah Pokok Bahasan</b></h4>
      </div>
      <form action='../guru/fungsi.php' method='POST'>
      <div class="modal-body"> 	
        <input type="hidden" name="funct" value="add_pb">
        <table>
          <tr><td>Pokok Bahasan yang ditambah</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="pb" class="form-control" required="required" style="width:300px;" autofocus></td></tr>
          <tr><td>dari Mata Pelajaran</td><td>&nbsp;:&nbsp;</td><td>
            <select name="uid_mapel">
              <?php
                session_start();
                include "../config/koneksi.php";
                $q=mysqli("SELECT t1.uid_mapel, t1.nama_mapel FROM tb_mapel t1 JOIN tb_guru_mapel t2 ON t1.uid_mapel=t2.uid_mapel WHERE t2.uid_user='".$_SESSION['id_user']."'");
                while ($a=mysqli_fetch_array($q)){
                  echo "<option value='".$a['uid_mapel']."'>".$a['nama_mapel']."</option>";
                }
              ?>    
            </select>
          </td></tr>
          <tr><td>untuk Jenjang</td><td>&nbsp;:&nbsp;</td><td>
            <select name="jenjang">
              <option value="sd">SD</option>
              <option value="smp">SMP</option>
              <option value="sma" selected="selected">SMA</option>
            </select>
          </td></tr>
        </table>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  	</form>
    </div>
  </div>
