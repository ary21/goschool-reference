       <div class="input-group">
        <div class="input-group-addon">
         <span class="glyphicon glyphicon-calendar"></span>
        </div>
        <input class="form-control" id="start_date" name="start_date" placeholder="dd/mm/yyyy" type="text"/>
       </div>

<!-- Extra JavaScript/CSS added manually in "Settings" tab -->
<script>
	$(document).ready(function(){
		var date_input=$('input[name="start_date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'dd/mm/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>
