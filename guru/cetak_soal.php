<!DOCTYPE html>
<html>
<head>
     <title></title>
     <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
     <link rel="stylesheet" href="css/style.css">
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)'],['<span class="math-tex">\\(','\\)</span>']]}
      });
    </script>
    <script type="text/javascript" async src="../plugins/MathJax/MathJax.js?config=TeX-MML-AM_CHTML"></script> 
</head>
<body style="font-family: serif; font-size: 12pt;">
<?php
include "../config/koneksi.php";        
$datates=mysqli_fetch_array(mysqli("SELECT t2.jenjang, t2.mode_tampil, t2.waktu, t2.nama_tes, t3.nama_mapel FROM tb_tes t2 JOIN tb_mapel t3 ON t2.uid_mapel=t3.uid_mapel WHERE t2.uid_tes='".$_REQUEST['uid_tes']."'"));
   $q=mysqli("SELECT uid_soaltes, uid_soal, layout_soal FROM tb_soaltes WHERE uid_tes='".$_REQUEST['uid_tes']."' ORDER BY id_soaltes");

echo "<div>
<h3>".$datates['nama_mapel']."-".$datates['nama_tes']." (".$datates['waktu']."menit)</h3>
<br>";
$no=1;
while ($a=mysqli_fetch_array($q)) {
    if($datates['jenjang']=='sma'){
        $pilihan=array("kunci", "pengecoh_1", "pengecoh_2", "pengecoh_3", "pengecoh_4");
        $abcde=array("A","B","C","D","E");
    }else{
        $pilihan=array("kunci", "pengecoh_1", "pengecoh_2", "pengecoh_3");
        $abcde=array("A","B","C","D");
    }
     shuffle($pilihan);
     $a_soal=mysqli_fetch_array(mysqli("SELECT soal, kunci, pengecoh_1, pengecoh_2, pengecoh_3, pengecoh_4 FROM tb_soal WHERE uid_soal='".$a['uid_soal']."'"));     

     echo "
     <table>";
     if($datates['jenjang']=='sma'){
          if($a['layout_soal']=="1"){
               echo "<tr><td rowspan='7' valign='top' width='60px' style='font-size: 24pt;color: red;'>".$no.".</td>";
               echo "<td colspan='2'>".$a_soal['soal']."</td></tr>";
               for ($i=0; $i < count($pilihan); $i++) { 
                    echo "<tr><td width='20px' valign='top'>".$abcde[$i].".</td><td valign='middle' >".$a_soal[$pilihan[$i]]."</td></tr>";
               }
          }else if($a['layout_soal']=="2"){
               echo "<tr><td rowspan='5' valign='top' width='60px' style='font-size: 24pt;color: red;'>".$no.".</td>";
               echo "<td colspan='4'>".$a_soal['soal']."</td></tr>";
               echo "<tr><td width='20px' valign='top'>".$abcde[0].".</td><td valign='middle' style='width:250px;' >".$a_soal[$pilihan[0]]."</td><td width='20px' valign='top'>".$abcde[3].".</td><td valign='middle'>".$a_soal[$pilihan[3]]."</td></tr>";
               echo "<tr><td width='20px' valign='top'>".$abcde[1].".</td><td valign='middle' style='width:250px;' >".$a_soal[$pilihan[1]]."</td><td width='20px' valign='top'>".$abcde[4].".</td><td valign='middle'>".$a_soal[$pilihan[4]]."</td></tr>";
               echo "<tr><td width='20px' valign='top'>".$abcde[2].".</td><td valign='middle' style='width:250px;' >".$a_soal[$pilihan[2]]."</td><td></td><td></td></tr>";
          }else if($a['layout_soal']=="3"){
               echo "<tr><td rowspan='4' valign='top' width='60px' style='font-size: 24pt;color: red;'>".$no.".</td>";
               echo "<td colspan='8'>".$a_soal['soal']."</td></tr>";
               echo "<tr><td width='20px' valign='top'>".$abcde[0].".</td><td valign='middle' style='width:180px;'>".$a_soal[$pilihan[0]]."</td><td width='20px' valign='top'>".$abcde[2].".</td><td valign='middle' style='width:180px;' >".$a_soal[$pilihan[2]]."</td><td width='20px' valign='top'>".$abcde[4].".</td><td valign='middle'>".$a_soal[$pilihan[4]]."</td></tr>";
               echo "<tr><td width='20px' valign='top'>".$abcde[1].".</td><td valign='middle' style='width:180px;' >".$a_soal[$pilihan[1]]."</td><td width='20px' valign='top'>".$abcde[3].".</td><td valign='middle' style='width:180px;' >".$a_soal[$pilihan[3]]."</td><td width='20px' valign='top'></td><td></td></tr>";
               
          }
      }else{
          if($a['layout_soal']=="1"){
               echo "<tr><td rowspan='7' valign='top' width='60px' style='font-size: 24pt;color: red;'>".$no.".</td>";
               echo "<td colspan='2'>".$a_soal['soal']."</td></tr>";
               for ($i=0; $i < count($pilihan); $i++) { 
                    echo "<tr><td width='20px' valign='top'>".$abcde[$i].".</td><td>".$a_soal[$pilihan[$i]]."</td></tr>";
               }
          }else if($a['layout_soal']=="2"){
               echo "<tr><td rowspan='5' valign='top' width='60px' style='font-size: 24pt;color: red;'>".$no.".</td>";
               echo "<td colspan='5'>".$a_soal['soal']."</td></tr>";
               echo "<tr><td width='20px' valign='top'>".$abcde[0].".</td><td style='margin-bottom: 5pt; width:300px;' >".$a_soal[$pilihan[0]]."</td><td style='width:5%;' ></td><td width='20px' valign='top'>".$abcde[2].".</td><td style='width:300px;' >".$a_soal[$pilihan[2]]."</td></tr>";
               echo "<tr><td width='20px' valign='top'>".$abcde[1].".</td><td style='margin-bottom: 5pt; width:300px;' >".$a_soal[$pilihan[1]]."</td><td></td><td width='20px' valign='top'>".$abcde[3].".</td><td style='width:300px;' >".$a_soal[$pilihan[3]]."</td></tr>";
          }

      }
     echo "
     </table><br>";
$no=$no+1;
}
echo "</div>";
?>
</body>
</html>