<!-- Here's your modal begin -->
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
        <h4 class="modal-title">Upload Your Sound Here</h4>
        </div>
        <form id="frm" method="post" enctype="multipart/form-data" action="processSound.php">
        <div class="modal-body">
            <input type="file" name="file_sound" id="file_sound" />
        </div>
     <div class="modal-footer">
        <input type="submit" class="btn btn-default" data-dismiss="modal" value="Save">
    </div>
    </form>        
    </div>
  </div>
<!-- End of Modal -->
