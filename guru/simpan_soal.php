<?php
session_start();
if(!isset($_SESSION['id_user'])){
	echo "<script>document.location='index.php';</script>";
}
include "../config/koneksi.php";
$mysound=$_REQUEST['sound'];
if($mysound!="No"){
	if(!empty($_FILES['file']) && $_FILES['file']['error'] != 4 && $_FILES['file'] != ''){
		$valid_formats = array("mp3");
		$name=$_FILES['file']['name'];
		$size=$_FILES['file']['size'];
		$ext = getExtension($name);
		if(in_array($ext,$valid_formats)){
			if($size<(2*1024*1024)){
				$path = "../sound_soal/";
				$actual_sound_name = time().substr(str_replace(" ", "_", $ext), 5).".".$ext;
				$tmp = $_FILES['file']['tmp_name'];
				if(move_uploaded_file($tmp, $path.$actual_sound_name)){
					$desc="File sound terupload";
					$file_sound=$path.$actual_sound_name;
					$sound="ada";
					$status="1";
				}else{
					$desc="File sound TIDAK terupload";
					$file_sound="";
					$sound="tidak ada";
					$status="0";
				}
			}else{
				$desc="Ukuran file LEBIH dari 2 Mb";
				$file_sound="";
				$sound="tidak ada";
				$status="0";
			}
		}else{
			$desc="Format file suara BUKAN mp3";
			$file_sound="";
			$sound="tidak ada";
			$status="0";
	}
	}else{
		$desc="Tidak ada File sound";
		$file_sound="";
		$sound="tidak ada";
		$status="0";
	}
}else{
	$desc="Tidak perlu sound";
	$file_sound="";
	$sound="tidak ada";
	$status="2";
}
if($status=="1" || $status=="2"){
	$q=mysqli("INSERT INTO tb_soal(uid_mapel, uid_pb, uid_user, soal, kunci, pengecoh_1, pengecoh_2, pengecoh_3, pengecoh_4, sound, file_sound) VALUES ('".$_SESSION['uid_mapel']."','".$_SESSION['uid_pb']."','".$_SESSION['id_user']."','".escape($_REQUEST['soal'])."','".escape($_REQUEST['key'])."','".escape($_REQUEST['dist1'])."','".escape($_REQUEST['dist2'])."','".escape($_REQUEST['dist3'])."','".escape($_REQUEST['dist4'])."','".$sound."','".$file_sound."')");
	if($q){
		echo "Soal Anda Berhasil disimpan!";
	}else{
		die(mysqli_error());
    	mysqli_close();
    	echo "Database Error ...<br>Soal Anda GAGAL disimpan!";
    	if($status=="1"){
    		unlink($file_sound);	
    	}
	}
}else{
	echo $desc."<br>Soal Anda GAGAL disimpan!";
}



function getExtension($str)
{ 
         $i = strrpos($str,".");
         if (!$i) { return ""; }
 
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }
?>