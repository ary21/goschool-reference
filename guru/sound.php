<!DOCTYPE html>
<html>
<head>
	<title>sound</title>
</head>
<body>
<!-- Important JS to make this project works -->
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/jquery.deskform.js"></script>
<script type="text/javascript" src="js/jquery.migrate-1.2.1.min.js"></script>

<!-- End of Important JS -->

Name: <input type="text" id="name" name="name"><br>
File: <input type="file" id="sound_file" name="sound_file"><br>
<button id="submit">Submit</button>
</body>

<script type="text/javascript">
	$('#submit').click(function() {
    var file_data = $('#sound_file').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('name', $("#name").val());
    $.ajax({
        url: 'sound_proccess.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(result){
            alert(result);
        }
    });
});
</script>
</html>