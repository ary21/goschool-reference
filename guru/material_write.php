<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Penulisan Materi</title>
    <link rel="shortcut icon" href="../dist/img/favicon.png" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)'],['<span class="math-tex">\\(','\\)</span>']]}
      });
    </script>
    <script type="text/javascript" async src="../plugins/MathJax/MathJax.js?config=TeX-MML-AM_CHTML"></script>    
</head>
<body style="background: #F7EFFF">
<?php
session_start();
include "../config/koneksi.php";
$materi=mysqli_fetch_assoc(mysqli("
        SELECT t1.topic, t1.status, t1.reader, t2.nama_mapel, t1.topic, t1.material_content, t1.upload, t1.youtube, t1.link_youtube
        FROM tb_material t1 
        JOIN tb_mapel t2 ON t1.uid_mapel=t2.uid_mapel
        WHERE t1.uid_material='".$_REQUEST['uid_material']."'"));

?>
<!-- Important JS to make this project works -->
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/jquery.deskform.js"></script>
<script type="text/javascript" src="js/jquery.migrate-1.2.1.min.js"></script>

<!-- End of Important JS -->
<div class="top">
    <table style="margin: 0px 15px 0px 15px;width: 99%"><tr><td width="30%" style="padding-top: 2px;text-align: left;font-size: 1.5em;color: white"><span class="glyphicon glyphicon-th"></span><span class="logo-lg"> SiABS Sekolah</span></td><td style="text-align:center;font-size: 2.2em;color: yellow"><span class="logo-lg"><b>Menulis Materi</b></span></td><td width="30%"  style="text-align: right;padding-right: 10px; font-size: 1.5em;color: white"><?php echo $_SESSION['surename'];?></td></tr></table>
</div>  
<div class="article">
<table style="width: 100%">
    <tr><td width="200px">Mata Pelajaran</td><td width="10px">&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><b><?php echo $materi['nama_mapel'];?></b></td><td rowspan="2" style="text-align: right;"><button class="btn btn-danger"  onclick="materi_close()"><span class="glyphicon glyphicon-remove"></span></button></td></tr>
    <tr><td>Topik Materi</td><td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><input type="text" id="topic" class="form-control" value='<?php echo $materi['topic'];?>'></td></tr>
</table>
<input type="hidden" id="uid_material" value="<?php echo $_REQUEST['uid_material'];?>">
<br>
<!--EDITOR-->
  <div style="width: 80%; margin: auto;max-height: 480px;overflow: hidden;">
      <table  width="100%">
        <tr>
          <td valign="top" colspan="3">
            <textarea id="ckeditor1" name="description" class="form-control">
              <?php
                echo $materi['material_content'];
              ?>
            </textarea>
          </td>
        </tr>
        <tr>
          <td valign='top' colspan='3'>
            <div class='input-group col-sm-12'>
              <span class='input-group-addon'>Youtube</span>
              <input type='text' id='link_youtube' class='form-control' placeholder='Link ...' value="<?php echo $materi['link_youtube'];?>">
            </div>
          </td>
        </tr>
        <tr><td valign="top" colspan="3">&nbsp;</td></tr>
        <tr style="text-align: center;">
          <td>
              <a href="" class="btn btn-default prev_materi" data-toggle="modal" id="prev1"><span class="glyphicon glyphicon-eye-open"></span> Preview</a>
          </td>
          <td>
              <button class="btn btn-primary" id="simpan" onclick="save_data()"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
          </td>
          <td>
            <a href="" class="btn btn-default upload-form-modal" data-toggle="modal"><span class="glyphicon glyphicon-open"></span> Image</a>
          </td>
        </tr>
        </table>
        <br><br>
      </div>
 
<div class="footer" style="font-family: sans-serif;text-align: right;"><strong>&copy; <?php echo date('Y'); ?> Enlarge by <a href="#">Ismuji and A4 Team</a>.</strong> All rights reserved.</div>
<!-- End of form -->
<!--/MODAL -->
    <div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left: 50%;">
    </div>
    <div class="modal fade" id="prev_materi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div>

    <div class="modal fade" id="modal_alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width:300px;height: 300px;">
        <div class="modal-content">
          <p><div id="message" style="font-size: 18px;text-align: center;font-weight: bold;color: blue;"></div></p>
          <p style="text-align: center;"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></p>
        </div>
      </div>
  </div>

<!-- SCRIPT ADD IMAGE -->
<script type="text/javascript">
  $(document).ready(function (){
    $(".upload-form-modal").click(function (e){
        var m = $(this).attr("id");
        $.ajax({
          url: "modal_image.php",
          type: "GET",
          data : {id: m,},
          success: function (ajaxData){
              $("#ModalUtama").html(ajaxData);
              $("#ModalUtama").modal('show',{backdrop: 'true'});
          }
        });
    });
  });
</script>

<!-- SCRIPT PREV SOAL --->
        <script>
            $(document).ready(function (){
                $(".prev_materi").click(function (e){
                  var prev_materi=[];
                    prev_materi[0] = document.getElementById("topic").value;
                    prev_materi[1] = CKEDITOR.instances.ckeditor1.getData();
                    $.ajax({
                        url: "modal_materi_prev.php",
                        type: "GET",
                        dataType: 'html',
                        data : {materi: prev_materi,},
                        success: function (ajaxData){
                            $("#prev_materi").html(ajaxData);
                            $("#prev_materi").modal('show',{backdrop: 'true'});
                            MathJax.Hub.Queue(["Typeset",MathJax.Hub,'prev_materi']);
                        }
                    });
                });

            });

</script>
<script>
  function save_data(){
    var fd = new FormData();
    var uid_material=document.getElementById("uid_material").value;
    var topic = document.getElementById("topic").value;
    var materi = CKEDITOR.instances.ckeditor1.getData();
    var link_youtube = document.getElementById("link_youtube").value;
    fd.append('uid_material',uid_material);
    fd.append('topic', topic);
    fd.append('materi', materi);
    fd.append('link_youtube', link_youtube);
    $.ajax({
        method:"POST",
        url:"simpan_materi.php",    
        data: fd,  
        cache: false,
        contentType: false,
        processData: false,   
        success: function(data){
            if(data!="Materi Anda Berhasil disimpan!"){
                $("#message").html(data);
                $("#modal_alert").modal('show');
            }else{
                materi_close();            
            }
        }
    });
  }
</script>

<script type="text/javascript">
  function materi_close(){
    document.location="../view/preview.php?view=material_page";
  }
</script>

<script>
    function myFunction-r() {
    var x = document.getElementById("soal").value;  
    var myWindow = window.open("", "", "width=400,height=400");
    myWindow.document.write(x);
    }
</script>
<!-- CKEditor -->
<script src="../plugins/ckeditor/ckeditor.js"></script>
<!-- CKEditor Adapters --> 
<script src="js/ckeditor/adapters/jquery.js"></script>
<!-- CKEditor Adapters --> 

<script data-sample="1">
  CKEDITOR.replace( 'ckeditor1', {
    extraPlugins: 'mathjax,justify',
    mathJaxLib: '../plugins/mathjax/MathJax.js?config=TeX-AMS_HTML',
    width:'100%',
    height:'250px',
  } );
</script>    
<!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- CKE Image Uploader JS -->
<script src="js/cke-upload-image.js"></script>
</body>
</html>
