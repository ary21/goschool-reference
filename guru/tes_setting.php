<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
     <p  style="text-align:right;margin-right: 10px;"><a href="#" class="btn btn-primary modal_tes_add"><span class="glyphicon glyphicon-plus-sign"></span> Tambah Tes/Ulangan</a>&nbsp;&nbsp;&nbsp;</p>
       <div class="box-body">
      <table id="example3" class="table table-bordered table-striped">
        <thead>
          <tr>
<th style='width:20px' align="center">No</th>
<th>Jenjang/Mapel</th>
<th>Nama tes</th>
<th>Soal</th>
<th>Mode Ambil</th>
<th>Mode Tampil</th>
<th>Status</th>
<th>Waktu</th>
<th>Token</th>
<th style='width:400px' align="center"></th>
          </tr>
        </thead>
        <tbody>
      <?php
        $tampil=mysqli("SELECT jenjang, uid_tes, uid_mapel, nama_tes, waktu, mode_ambil, mode_tampil, status_tes, token, status_token, status_review, update_tes FROM tb_tes WHERE uid_user='".$_SESSION['id_user']."' ORDER BY update_tes DESC");
        $no = 1;
        while($r=mysqli_fetch_array($tampil)){
            $mapel=mysqli_fetch_array(mysqli("SELECT nama_mapel FROM tb_mapel WHERE uid_mapel='".$r['uid_mapel']."'"));
            if($r['mode_ambil']=="manual"){
                $n_soal=mysqli_num_rows(mysqli("SELECT uid_soaltes FROM tb_soaltes WHERE uid_tes='".$r['uid_tes']."'"));
            }else{
                $ns=mysqli_fetch_array(mysqli("SELECT SUM(jml_soal) AS ns FROM tb_soalrand WHERE uid_tes='".$r['uid_tes']."'"));
                $n_soal=$ns['ns'];
            }
            $n_pd=mysqli_num_rows(mysqli("SELECT id_datatested FROM tb_datatested WHERE uid_tes='".$r['uid_tes']."'"));
            echo "<tr>
            <td align='center'>".$no.".</td>
            <td><font color='red'><b>".strtoupper($r['jenjang']).":</b></font><br>".$mapel['nama_mapel']."</td>
            <td>".$r['nama_tes']."</td>
            <td>".$n_soal."</td>
            <td>".$r['mode_ambil']."</td>
            <td>".$r['mode_tampil']."</td>
            <td>";
                if($r['status_tes']=="open"){
                    echo "<font color='red'><b>Open</b></font>";
                }else{
                    echo "close";
                }
            echo "                
            </td>
            <td>".$r['waktu']." menit</td><td>";
            if($r['status_token']=="1"){
                echo $r['token'];
            }else{
                echo "<font color='red'>no Token</font>";
            }
            echo "</td>";
            echo " 
            <td style='width:200px;'>
            <a class='btn btn-success  btn-xs modal_tes_edit' id='".$r['uid_tes']."'><span class='glyphicon glyphicon-list'></span> Edit</a>";
            if($r['mode_ambil']!="random"){
                echo "
                &nbsp;
                <a href='preview.php?view=memilih_soal&data=".$r['uid_tes']."' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus-sign'></span> Soal</a>&nbsp;
                <a href='preview.php?view=setting_soal&data=".$r['uid_tes']."' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-print'></span></a>";
            }else{
                echo "
                <a href='preview.php?view=memilih_pb&data=".$r['uid_tes']."' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus-sign'></span> Soal</a>&nbsp;
                ";
            }
        if($n_pd==0){
            echo "&nbsp;<button class='btn btn-danger  btn-xs'  id='".$r['uid_tes']."' onclick='delete_data(this.id)'><span class='glyphicon glyphicon-trash'></span></button>";
        }
        echo "</td></tr>";
          $no++;
        }
      
      ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
    </div>
</div>
<!--/MODAL -->
<div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
</div>
<!-- DELETE TES -->
<script type="text/javascript">
function delete_data(id){
    $.ajax({
      url: "../guru/fungsi.php?funct=del_tes&uid_tes="+id,
      type: "GET",
      dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
}
</script>
<!-- SCRIPT ADD TES -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_tes_add").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/modal_tes_add.php",
                        type: "GET",
                        data : {uid_pb: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>
<!-- SCRIPT EDIT PB -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_tes_edit").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/modal_tes_edit.php",
                        type: "GET",
                        data : {uid_tes: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>
      
<!-- SCRIPT SOAL -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_add_soal").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/add_soal.php",
                        type: "GET",
                        data : {uid_pb: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>        
