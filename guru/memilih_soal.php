<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
    </div>
       <div class="box-body">
        <?php
            $tes=mysqli_fetch_array(mysqli("SELECT id_tes, uid_tes, jenjang, uid_user, uid_mapel, nama_tes, waktu, mode_ambil, mode_tampil, status_tes, update_tes FROM tb_tes WHERE uid_tes='".$data."'"));
        ?>
        <script type="text/javascript">
            $(document).ready(function(){ 
            $("#myTab a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
                });
            });
        </script>

<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#soal_tes">Soal Tes: <?php echo $tes['nama_tes'];?></a></li>
    <li><a href="#bank_soal">Bank Soal</a></li>
</ul> 

<div class="tab-content">

<div id="soal_tes"  class="tab-pane fade in active">
    <?php
    $soaltes=mysqli("SELECT t1.uid_soaltes, t2.jenjang, t3.soal, t3.kunci, t3.pengecoh_1, t3.pengecoh_2, t3.pengecoh_3, t3.pengecoh_4, t3.sound, t3.file_sound FROM tb_soaltes t1 JOIN tb_tes t2 ON t1.uid_tes=t2.uid_tes JOIN tb_soal t3 ON t1.uid_soal=t3.uid_soal WHERE t2.uid_tes='".$data."' ORDER BY t1.id_soaltes ASC");
    $no=1;
    while($a_soaltes=mysqli_fetch_array($soaltes)){
            echo "<table width='100%'>";
            echo "
            <tr><td width='60px' valign='top' style='font-size:24px;color:red;'><b>".$no.".</b><br><button class='btn btn-danger  btn-xs'  id='".$a_soaltes['uid_soaltes']."' onclick='del_soaltes(this.id)'><span class='glyphicon glyphicon-remove'></span></button></td>
                <td>";
                if($a_soaltes['sound']=="ada"){
                    echo "<audio controls><source src='".$a_soaltes['file_sound']."' type='audio/mpeg'>Your browser does not support the audio element.</audio><br>";
                }
                echo $a_soaltes['soal'];
                echo "
                <ol type='A'>
                <li style='color:red;'><b>".$a_soaltes['kunci']."</b></li>
                <li>".$a_soaltes['pengecoh_1']."</li>
                <li>".$a_soaltes['pengecoh_2']."</li>
                <li>".$a_soaltes['pengecoh_3']."</li>";
                if($a_soaltes['jenjang']=='sma'){
                    echo "<li>".$a_soaltes['pengecoh_4']."</li>";
                }
                echo "
                </ol>
                </td>
            <tr><td colspan='2'>
            <hr></td></tr>";
            $no=$no+1;
            }
        ?>
        </table>
</div>

<div id="bank_soal" class="tab-pane fade">
    <?php
        $soal=mysqli("SELECT t1.uid_soal, t1.uid_mapel, t1.soal, t1.kunci, t1.pengecoh_1, t1.pengecoh_2, t1.pengecoh_3, t1.pengecoh_4, t1.sound, t1.file_sound, t1.update_soal, t2.jenjang, t2.pb FROM tb_soal t1 JOIN tb_pb t2 ON t1.uid_pb=t2.uid_pb WHERE t1.uid_mapel='".$tes['uid_mapel']."' AND t1.uid_user='".$tes['uid_user']."' AND t1.uid_soal NOT IN (SELECT uid_soal FROM tb_soaltes WHERE uid_tes='".$data."') ORDER BY update_soal ASC");
    ?>
    <table id="example4" class="table bordered" width="100%">
<thead><tr><th width="130px"><input type="checkbox" onClick="toggle1(this)"/> All&nbsp;<button class='btn btn-success' onclick='add_soaltes()'><span class='glyphicon glyphicon-arrow-left'></span>Upload</button></th><th width="130px">Pokok/Sub Bahasan</th><th>Soal</th></tr></thead>
<tbody>
<?php

WHILE($a_soal=mysqli_fetch_array($soal)){
    echo "<tr>
    <td valign='top'><input type='checkbox' name='soal_tes' value='".$a_soal['uid_soal']."'></td>
    <td valign='top'><font color='blue'><b>".$a_soal['pb']."</b></font></td>
    <td>";
    if($a_soal['sound']=="ada"){
        echo "<audio controls><source src='".$a_soal['file_sound']."' type='audio/mpeg'>Your browser does not support the audio element.</audio><br>";
    }
    echo $a_soal['soal'];
    echo "
    <ol type='A'>
                <li style='color:red;'><b>".$a_soal['kunci']."</b></li>
                <li>".$a_soal['pengecoh_1']."</li>
                <li>".$a_soal['pengecoh_2']."</li>
                <li>".$a_soal['pengecoh_3']."</li>";
                if($a_soal['jenjang']=='sma'){
                    echo "<li>".$a_soal['pengecoh_4']."</li>";
                }
                echo "
                </ol>
    </td>
    </tr>";
}

?>
</tbody>
    </table>
</div>

</div>
</div><!-- /.box-body -->
</div>
</div>
<script type="text/javascript">
    function add_soaltes(){
     var uid_tes = "<?php echo $data; ?>";
     var uid_soal = $('input[name=soal_tes]:checked').map(function(){
                        return $(this).val();
                 }).get();
        $.ajax( {
        url: "../guru/fungsi.php?funct=add_soaltes&uid_tes="+uid_tes+"&uid_soal="+uid_soal,
        type: "POST",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
     }
    //==================
    function del_soaltes(uid_soaltes){
      $.ajax({
      url: "../guru/fungsi.php?funct=del_soaltes&uid_soaltes="+uid_soaltes,
      type: "POST",
      dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
    }

    function toggle1(source) {
        checkboxes = document.getElementsByName('soal_tes');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

</script>