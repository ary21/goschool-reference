  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Menambah Tes/Ulangan</b></h4>
      </div>
      <form action='../guru/fungsi.php' method='POST'>
      <div class="modal-body"> 	
        <input type="hidden" name="funct" value="add_tes">
        <table>
          <tr><td>Nama Tes/Ulangan</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="nama tes" class="form-control" required="required" style="width:300px;" autofocus></td></tr>
          <tr><td>dari Mata Pelajaran</td><td>&nbsp;:&nbsp;</td><td>
            <select name="uid_mapel">
              <?php
                session_start();
                include "../config/koneksi.php";
                $q=mysqli("SELECT t1.uid_mapel, t1.nama_mapel FROM tb_mapel t1 JOIN tb_guru_mapel t2 ON t1.uid_mapel=t2.uid_mapel WHERE t2.uid_user='".$_SESSION['id_user']."'");
                while ($a=mysqli_fetch_array($q)){
                  echo "<option value='".$a['uid_mapel']."'>".$a['nama_mapel']."</option>";
                }
              ?>    
            </select>
          </td></tr>
          <tr><td>Jenjang</td><td>&nbsp;:&nbsp;</td>
            <td>
              <select name="jenjang">
                <option value="sd">SD</option>
                <option value="smp">SMP</option>
                <option value="sma">SMA</option>
              </select>
            </td></tr>
          <tr><td>Waktu</td><td>&nbsp;:&nbsp;</td><td><input type="number" name="waktu" class="form-control"  style="width:150px;" placeholder="dalam menit"></td></tr>
          <tr><td>Mode Ambil soal</td><td>&nbsp;:&nbsp;</td><td><input type="radio" name="mode_ambil" value="random"> Random&nbsp;&nbsp;&nbsp;<input type="radio" name="mode_ambil" value="manual" checked="checked"> Manual</td></tr>
          <tr><td>Mode Tampilan Soal</td><td>&nbsp;:&nbsp;</td><td><input type="radio" name="mode_tampil" value="random" checked="checked"> Random&nbsp;&nbsp;&nbsp;<input type="radio" name="mode_tampil" value="normal"> Normal</td></tr>
          <tr><td>Status Tes</td><td>&nbsp;:&nbsp;</td><td><input type="radio" name="status_tes" value="close" checked="checked"> Close&nbsp;&nbsp;&nbsp;<input type="radio" name="status_tes" value="open"> Open</td></tr>
          <tr><td>Status Review</td><td>&nbsp;:&nbsp;</td><td><input type="radio" name="status_review" value="close" checked="checked"> Close&nbsp;&nbsp;&nbsp;<input type="radio" name="status_review" value="open"> Open</td></tr>
          <tr><td>Status Top20</td><td>&nbsp;:&nbsp;</td><td><input type="radio" name="status_top20" value="close" checked="checked"> Close&nbsp;&nbsp;&nbsp;<input type="radio" name="status_top20" value="open"> Open</td></tr>
          <tr><td>Token</td><td>&nbsp;:&nbsp;</td><td><input type="radio" name="status_token" value="1" checked="checked"> Pakai token&nbsp;&nbsp;&nbsp;<input type="radio" name="status_token" value="0"> Otomatis</td></tr>
          <tr><td>Hasil tes</td><td>&nbsp;:&nbsp;</td><td><input type="radio" name="hasil_tes" value="1" checked="checked"> Terlihat&nbsp;&nbsp;&nbsp;<input type="radio" name="hasil_tes" value="0"> Tidak terlihat</td></tr>
          <tr>
            <td>Status Re-Test</td>
            <td>&nbsp;:&nbsp;</td>
            <td>
              <input type="radio" name="retest" value="1"> Diijinkan&nbsp;&nbsp;&nbsp;
              <input type="number" name="n_retest" value="2" min="1" max="5" style="width:40px" onkeydown="return false"> kali&nbsp;&nbsp;&nbsp;
              <input type="radio" name="retest" value="0" checked="checked"> Tidak diijinkan
            </td>
          </tr>
        </table>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  	</form>
    </div>
  </div>
<script>
  $(document).ready(function(){
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    $("#start_tes, #end_tes").datepicker({
      format: 'dd/mm/yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
    })
  })
</script>  