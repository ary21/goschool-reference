<?php
session_start();
if(!isset($_SESSION['id_user'])){
	echo "<script>document.location='index.php';</script>";
}
include "../config/koneksi.php";
$mysound=$_REQUEST['add_sound'];
if($mysound!="No"){
	if(!empty($_FILES['file']) && $_FILES['file']['error'] != 4 && $_FILES['file'] != ''){
		if($_REQUEST['sound']=="ada"){
			unlink($_REQUEST['file_sound']);
		}
		$valid_formats = array("mp3");
		$name=$_FILES['file']['name'];
		$size=$_FILES['file']['size'];
		$ext = getExtension($name);
		if(in_array($ext,$valid_formats)){
			if($size<(2*1024*1024)){
				$path = "../sound_soal/";
				$actual_sound_name = time().substr(str_replace(" ", "_", $ext), 5).".".$ext;
				$tmp = $_FILES['file']['tmp_name'];
				if(move_uploaded_file($tmp, $path.$actual_sound_name)){
					$desc="File sound terupload";
					$file_sound=$path.$actual_sound_name;
					$sound="ada";
					$status="1";
				}else{
					$desc="File sound TIDAK terupload";
					$file_sound="";
					$sound="tidak ada";
					$status="0";
				}
			}else{
				$desc="Ukuran file LEBIH dari 2 Mb";
				$file_sound="";
				$sound="tidak ada";
				$status="0";
			}
		}else{
			$desc="Format file suara BUKAN mp3";
			$file_sound="";
			$sound="tidak ada";
			$status="0";
	}
	}else{
		$desc="Tidak ada File sound";
		$file_sound="";
		$sound="ada";
		$status="3";
	}
}else{
	$desc="Tidak perlu sound";
	$file_sound="";
	$sound="tidak ada";
	$status="2";
}
if($status=="1" || $status=="2"){
	$q=mysqli("UPDATE tb_soal SET
		soal='".escape($_REQUEST['soal'])."',
		kunci='".escape($_REQUEST['key'])."',
		pengecoh_1='".escape($_REQUEST['dist1'])."',
		pengecoh_2='".escape($_REQUEST['dist2'])."', 
		pengecoh_3='".escape($_REQUEST['dist3'])."', 
		pengecoh_4='".escape($_REQUEST['dist4'])."',
		sound='".$sound."',
		file_sound='".$file_sound."'
		WHERE uid_soal='".$_REQUEST['uid_soal']."'");

	if($q){
		echo "Soal Anda Berhasil disimpan!";
	}else{
		die(mysqli_error());
    	mysqli_close();
    	echo "Database Error ...<br>Soal Anda GAGAL disimpan!";
    	if($status=="1"){
    		unlink($file_sound);	
    	}
	}
}elseif($status=="3"){
	$q=mysqli("UPDATE tb_soal SET soal='".escape($_REQUEST['soal'])."', kunci='".escape($_REQUEST['key'])."', pengecoh_1='".escape($_REQUEST['dist1'])."', pengecoh_2='".escape($_REQUEST['dist2'])."', pengecoh_3='".escape($_REQUEST['dist3'])."', pengecoh_4='".escape($_REQUEST['dist4'])."'
		WHERE uid_soal='".$_REQUEST['uid_soal']."'");
		if($q){
			echo "Soal Anda Berhasil disimpan!";
		}else{
			die(mysqli_error());
    		mysqli_close();
    		echo "Database Error ...<br>Soal Anda GAGAL disimpan!";
		}

}else{
	echo $desc."<br>Soal Anda GAGAL disimpan!";
}

function getExtension($str)
{ 
         $i = strrpos($str,".");
         if (!$i) { return ""; }
 
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }
/*
if(count($_REQUEST['mydata'])==7){
	$q=mysqli("UPDATE tb_soal SET 
		soal='".mysql_escape_string($_REQUEST['mydata'][0])."',
		kunci='".mysql_escape_string($_REQUEST['mydata'][1])."',
		pengecoh_1='".mysql_escape_string($_REQUEST['mydata'][2])."',
		pengecoh_2='".mysql_escape_string($_REQUEST['mydata'][3])."',
		pengecoh_3='".mysql_escape_string($_REQUEST['mydata'][4])."',
		pengecoh_4='".mysql_escape_string($_REQUEST['mydata'][5])."' 
		WHERE uid_soal='".$_REQUEST['mydata'][6]."'");
}else{
	$q=mysqli("UPDATE tb_soal SET 
		soal='".mysql_escape_string($_REQUEST['mydata'][0])."',
		kunci='".mysql_escape_string($_REQUEST['mydata'][1])."',
		pengecoh_1='".mysql_escape_string($_REQUEST['mydata'][2])."',
		pengecoh_2='".mysql_escape_string($_REQUEST['mydata'][3])."',
		pengecoh_3='".mysql_escape_string($_REQUEST['mydata'][4])."',
		pengecoh_4='' 
		WHERE uid_soal='".$_REQUEST['mydata'][5]."'");	
}
if($q){
	echo "Ok";
}else{
	echo "Gagal Update";
}
*/
?>