  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Editing Pokok Bahasan</b></h4>
      </div>
      <?php
        session_start();
        include "../config/koneksi.php";
        $a1=mysqli_fetch_array(mysqli("SELECT t1.jenjang, t1.pb, t2.uid_mapel, t2.nama_mapel FROM tb_pb t1 JOIN tb_mapel t2 ON t1.uid_mapel=t2.uid_mapel WHERE t1.uid_pb='".$_REQUEST['uid_pb']."'"));
        $q2=mysqli("SELECT t1.uid_mapel, t1.nama_mapel FROM tb_mapel t1 JOIN tb_guru_mapel t2 ON t1.uid_mapel=t2.uid_mapel WHERE t2.uid_user='".$_SESSION['id_user']."' AND t1.uid_mapel !='".$a1['uid_mapel']."'");
      ?>    
      <form action='../guru/fungsi.php' method='POST'>
      <div class="modal-body"> 	
        <input type="hidden" name="funct" value="edit_pb">
        <input type="hidden" name="uid_pb" value="<?php echo $_REQUEST['uid_pb'];?>">
        <table>
          <tr><td>Pokok Bahasan</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="pb" value="<?php echo $a1['pb'];?>" class="form-control" required="required" style="width:300px;" autofocus></td></tr>
          <tr><td>dari Mata Pelajaran</td><td>&nbsp;:&nbsp;</td><td>
            <select name="uid_mapel" selected>
              <option value="<?php echo $a1['uid_mapel'];?>"><?php echo $a1['nama_mapel'];?></option>
              <?php
                while ($a2=mysqli_fetch_array($q2)){
                  echo "<option value='".$a2['uid_mapel']."'>".$a2['nama_mapel']."</option>";
                }
              ?>    
            </select>
          </td></tr>
          <tr><td>untuk Jenjang</td><td>&nbsp;:&nbsp;</td><td>
            <select name="jenjang" selected>
              <option value="<?php echo $a1['jenjang'];?>"><?php echo strtoupper($a1['jenjang']);?></option>
              <option value="sd">SD</option>
              <option value="smp">SMP</option>
              <option value="sma">SMA</option>
            </select>
          </td></tr>
        </table>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  	</form>
    </div>
  </div>
