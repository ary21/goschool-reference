<!-- Here's your modal begin -->
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
        <h4 class="modal-title">Upload Your Image Here</h4>
        </div>
        <div class="modal-body">
        <p><div id="displayImg"></div></p>

        <form id="frm" method="post" enctype="multipart/form-data" action="processImage.php">
            <input type="hidden" id="myinput" value="<?php echo $_REQUEST['id'];?>">
        <div id="imgLoading" style="display:none">
        <img src="loading.gif" alt="Uploading...."/>
        </div>
        <div id="ingLoadButton">
        <input type="file" name="deskImg" id="deskImg" />
        </div>
        <p><hr />hint : click image to insert into CKEditor.</p>
        </form>
        </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>        
    </div>
  </div>
<!-- End of Modal -->
