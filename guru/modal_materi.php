<!-- Here's your modal begin -->
  <div class="modal-dialog" id="modal-dialog" style="width: 90%;height: 90%; overflow: hidden;margin-top:10px">
    <?php
    include "../config/koneksi.php";
    $dat=$_REQUEST['uid_material'];
    $materi=mysqli_fetch_assoc(mysqli("
        SELECT t1.uid_user, t1.topic, t1.status, t1.reader, t2.nama_mapel, t1.topic, t1.material_content, t1.upload, t1.youtube, t1.link_youtube, t1.uid_mapel, t3.sure_name, t3.level
        FROM tb_material t1 
        JOIN tb_mapel t2 ON t1.uid_mapel=t2.uid_mapel
        JOIN tb_users t3 ON t1.uid_user=t3.uid_user
        WHERE t1.uid_material='".$dat."'"));
    $uid_material=$dat;
    $uid_user=$materi['uid_user'];
    $status_user=$materi['level'];
    $sure_name=$materi['sure_name'];
    ?>
    <div class="modal-content">
        <div class="modal-header" style="padding:5px;">
            <button data-dismiss="modal" class="close" type="button" style="font-size: 36pt;color:black;">&times;</button>
            <h3 class="modal-title"><b><?php echo $materi['topic'];?></b></h3>
        </div>
        <div class="row">
            <div class="column left modal-body" style="font-family: sans-serif; overflow-x: hidden; font-size: 18px; overflow-y: auto;">
                <!--PETUNJUK-->
                <div class="alert" role="alert" style="background-color: #c4cfff;">
                    <b>Petunjuk Belajar:</b>
                    <ul>
                        <li>Baca dan pahami secara seksama materi pembelajaran yang diberikan.</li>
                        <li>Bila ada kesulitan, ayo kita budayakan untuk mendiskusikan di bagian tanya jawab.</li>
                        <li>Kerjakan soal/latihan/tugas yang diberikan.</li>
                        <li>Bila diperlukan mengirim file, gambar, dll silakan gunakan fasilitas upload file.</li>
                        <li>Berikan rate tingkat pemahaman agar Guru dapat memberikan materi pembelajaran lanjutan kepada Anda.</li>
                    </ul>
                </div>
                <!--MATERI PEMBELAJARAN-->
                <div class="alert" role="alert" style="background-color: #c4cfff;">
                    <b>Materi Pembelajaran</b>
                </div>
                <?php
                    echo $materi['material_content'];
                    if(strlen($materi['link_youtube'])!=0){
                        echo "<br>
                        <div class='embed-responsive embed-responsive-16by9'>
                            <iframe allowfullscreen='' class='embed-responsive-item' src=".str_replace("watch?v=","embed/",$materi['link_youtube']).">
                            </iframe>
                        </div>";
                    }
                    if($materi['upload']!=0){
                        echo "
                        <br>
                        <div class='alert' role='alert' style='background-color: #c4cfff;'>
                            <b>Upload Tugas</b>
                        </div>
                        <div class='col-lg-4'>
                            <div class='input-group'>
                                <input type='file' class='form-control'>
                                <span class='input-group-btn'>
                                    <button class='btn btn-primary' type='button'>Upload</button>
                                </span>
                            </div>
                        </div>";
                    }
                ?>
            </div>
            <div class="column right">
                <div style="background-color: green; color: white; font-size: 16pt; margin-bottom:10px; text-align: center">
                    <b>Tanya Jawab</b>
                </div>
                <div class="parent" id="messages-box" style="font-size: 14px; overflow-y: auto; overflow-x: hidden; height:100%;">
                    <div class="direct-chat-messages" id="chats"></div>
                </div>
                <div>
                  <div class="input-group input-group-lg">
                    <textarea class="form-control" style="font-size: 14px;" id="comment" placeholder="Type message..." rows="3"></textarea>
                    <div class="input-group-btn">
                      <button onclick="kirim_comment()" type="button" class="btn" style="background-color: green; color: white">Submit</button>
                    </div>

                  </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="background-color: #EBEAEA">
        </div>        
    </div>
  </div>
<!-- End of Modal -->
<style type="text/css">
    .column {float: left; max-height: 450px;}
    .left {width: 70%; padding-left: 30px;}
    .right {width: 30%; padding-right: 20px;}
    .row:after {content: ""; display: table; clear: both;}
    .parent {display: flex; flex-direction: column; justify-content: space-between;}
    .direct-chat-messages{height:360px;}
    h4{color:red;}
</style>
<script type="text/javascript">
  var id_comment;
  var uid_material="<?php echo $uid_material;?>";
  var uid_user="<?php echo $uid_user;?>";
  var status_user="<?php echo $status_user;?>";
  var sure_name="<?php echo $sure_name;?>";
  chat_history();
//
  function chat_history(){
    $.post('../guru/chat_history.php', {'uid_material': uid_material}, function(data) {
        $(data).hide().appendTo('.direct-chat-messages').fadeIn();
        var n=$(".direct-chat-messages")[0].scrollHeight;
        scroll_top(n);
    });
    return false;
  }
//
  function kirim_comment(){
    var comment=$("#comment").val();
    if(comment!=""){
    $.post('../guru/chat_send.php', {'uid_material': uid_material, 'uid_user':uid_user, 'status_user': status_user, 'sure_name': sure_name, 'comment': comment}, function(data){
        var n=$(".direct-chat-messages")[0].scrollHeight;
        scroll_top(n);
    });
    }
    return false;
  }
//
    $('#comment').on('keypress', function(e) {
        if (e.keyCode === 13) {
            kirim_comment();
            $("#comment").val('');
            $("#comment").focus();
        }
    });
//
  setInterval(function(){
    chat_coming(id_comment);
    MathJax.Hub.Queue(["Typeset",MathJax.Hub,'chats']);
  }, 1000);
//
  function chat_coming(id_comment){
    $.post('../guru/chat_coming.php', {'id_comment': id_comment, 'uid_material': uid_material}, function(data) {
      if(data.length!=0){
          $(data).hide().appendTo('.direct-chat-messages').fadeIn();
      }
    });
    return false;
  }
//
    function scroll_top(n){
        $(".direct-chat-messages").scrollTop(n);
    }
</script>
