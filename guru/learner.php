<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
    </div>
       <div class="box-body">
        <?php
            $topic=mysqli_fetch_array(mysqli("SELECT topic FROM tb_material WHERE uid_material='".$data."'"));
        ?>
        <script type="text/javascript">
            $(document).ready(function(){ 
            $("#myTab a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
                });
            });
        </script>
<p><b><?php echo $topic['topic'];?></b></p>
<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#pd_all">PD Semua</a></li>
    <li><a href="#pd_belum">PD Belum</a></li>
    <li><a href="#pd_sudah">PD Sudah</a></li>
</ul> 

<div class="tab-content">
          <!--TAB 1-->
          <div id="pd_all"  class="tab-pane fade in active">
            <?php
              $qpd_kelas=mysqli("SELECT t2.sure_name, t3.nama_kelas, t1.uid_user FROM tb_rombel t1 JOIN tb_users t2 ON t1.uid_user=t2.uid_user JOIN tb_datakelas t3 ON t1.id_kelas=t3.id_kelas WHERE t2.uid_user NOT IN (SELECT uid_user FROM tb_learned WHERE uid_material='".$data."') ORDER BY t3.nama_kelas,t2.sure_name ASC");
            ?>
            <table id="example4" width="100%" class="table bordered">
              <thead>
              <tr><th>No.</th><th>Nama</th><th>Kelas</th><th><input type="checkbox" onClick="toggle1(this)"/> All &nbsp;&nbsp;&nbsp;<button class="btn btn-success" onclick="input_learner()"><span class="glyphicon glyphicon-log-in"></span> Input Learner</button></th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_kelas=mysqli_fetch_array($qpd_kelas)) {
                echo "<tr><td>".$no."</td><td>".$apd_kelas['sure_name']."</td><td>".$apd_kelas['nama_kelas']."</td>
                <td><input type='checkbox' name='pd_in' value='".$apd_kelas['uid_user']."'></td></tr>";
                $no++;
              }
              
              ?>
              </tbody>
            </table>
          </div>

          <!--TAB 2-->
          <div id="pd_belum"  class="tab-pane fade">
            <?php
              $qpd_learned=mysqli("SELECT t1.uid_learned, t2.sure_name, t4.nama_kelas FROM tb_learned t1 JOIN tb_users t2 ON t1.uid_user=t2.uid_user JOIN tb_rombel t3 ON t1.uid_user=t3.uid_user JOIN tb_datakelas t4 ON t3.id_kelas=t4.id_kelas WHERE t1.uid_material='".$data."' AND t1.open='0' ORDER BY t4.nama_kelas,t2.sure_name ASC");
            ?>
            <table id="example5" class="table table-bordered table-striped">
              <thead>
              <tr><th>No.</th><th>Nama</th><th>Kelas</th><th style="width: 300px;"><input type="checkbox" onClick="toggle2(this)"/> All &nbsp;&nbsp;&nbsp;<button class="btn btn-danger" onclick="outpdfromlearn()"><span class="glyphicon glyphicon-plus-sign"></span> Keluarkan PD</button>
              </th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_learned=mysqli_fetch_array($qpd_learned)) {
                echo "<tr><td>".$no."</td><td>".$apd_learned['sure_name']."</td><td>".$apd_learned['nama_kelas']."</td><td><input type='checkbox' name='pd_learned' value='".$apd_learned['uid_learned']."'></td></tr>";
                $no++;
              }
              
              ?>
              </tbody>
            </table>            
          </div>

          <!--TAB 3-->
          <div id="pd_sudah"  class="tab-pane fade">
            <?php
              $qpd_open=mysqli("SELECT t1.uid_learned, t2.sure_name, t4.nama_kelas, t1.status_learned, t1.upload, t1.link_upload, t1.time_open FROM tb_learned t1 JOIN tb_users t2 ON t1.uid_user=t2.uid_user JOIN tb_rombel t3 ON t1.uid_user=t3.uid_user JOIN tb_datakelas t4 ON t3.id_kelas=t4.id_kelas WHERE t1.uid_material='".$data."' AND t1.open='1' ORDER BY t4.nama_kelas,t2.sure_name ASC");
            ?>
            <table id="example5" class="table table-bordered table-striped">
              <thead>
              <tr><th>No.</th><th>Nama</th><th>Kelas</th><th>Waktu Buka</th><th>Rate</th><th>Upload</th><th>Link</th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_open=mysqli_fetch_array($qpd_open)) {
                echo "<tr>
                <td>".$no."</td>
                <td>".$apd_open['sure_name']."</td>
                <td>".$apd_open['nama_kelas']."</td>
                <td>".$apd_open['time_open']."</td>
                <td>";
                $star=array("white_star","1","2","3","4","gold_star");
                $img="<img src='../images/".$star[$apd_open['status_learned']].".png'>";
                echo $img;
                echo "</td><td>";
                if($apd_open['upload']!=0){
                  echo "<span class='glyphicon glyphicon-ok'></span>";
                  echo "</td><td><a href='../file_uploaded/".$apd_open['link_upload']."' target='_blank'>Buka</a></td></tr>";
                }else{
                  echo "</td><td></td></tr>";
                }
                $no++;
              }
              ?>
              </tbody>
            </table>            
          </div>  
          </div>

        </div> 
</div>
</div><!-- /.box-body -->
<script type="text/javascript">
    function input_learner(){
     var uid_material = "<?php echo $data; ?>";
     var uid_user = $('input[name=pd_in]:checked').map(function(){
                        return $(this).val();
                 }).get();
        $.ajax( {
        url: "../guru/fungsi.php?funct=input_learner&uid_material="+uid_material+"&uid_user="+uid_user,
        type: "GET",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
     }
    //==================
    function outpdfromlearn(){
     var uid_learned = $('input[name=pd_learned]:checked').map(function(){
                        return $(this).val();
                 }).get();
        $.ajax( {
        url: "../guru/fungsi.php?funct=outpdfromlearn&uid_learned="+uid_learned,
        type: "GET",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
    }

    function toggle1(source) {
        checkboxes = document.getElementsByName('pd_in');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    function toggle2(source) {
        checkboxes = document.getElementsByName('pd_learned');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    function retest(id){
      $.ajax( {
        url: "../guru/fungsi.php?funct=reset_tested&uid_tested="+id,
        type: "GET",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
    }

    function reset(id){
      $.ajax( {
        url: "../guru/fungsi.php?funct=reset_test&uid_tested="+id,
        type: "GET",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
    }

</script>