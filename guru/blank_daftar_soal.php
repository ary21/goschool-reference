<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Daftar Soal</title>
    <link rel="shortcut icon" href="../dist/img/favicon.png" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <!-- DataTables -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">    
    <link rel="stylesheet" href="css/style.css">
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)'],['<span class="math-tex">\\(','\\)</span>']]}
      });
    </script>
    <script type="text/javascript" async src="../plugins/MathJax/MathJax.js?config=TeX-MML-AM_CHTML"></script>    
</head>
<body style="background: #F7EFFF">
<?php
session_start();
include "../config/koneksi.php";
$nama_pb=mysqli_fetch_array(mysqli("SELECT jenjang, uid_mapel, pb FROM tb_pb WHERE uid_pb='".$_REQUEST['uid_pb']."'"));
$nama_mapel=mysqli_fetch_array(mysqli("SELECT nama_mapel FROM tb_mapel WHERE uid_mapel='".$nama_pb['uid_mapel']."'"));
$q=mysqli("SELECT uid_soal, uid_pb, soal, kunci, pengecoh_1, pengecoh_2, pengecoh_3, pengecoh_4, sound, file_sound, update_soal FROM tb_soal WHERE uid_pb='".$_REQUEST['uid_pb']."'");
?>
<!-- Important JS to make this project works -->
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/jquery.deskform.js"></script>
<script type="text/javascript" src="js/jquery.migrate-1.2.1.min.js"></script>

<!-- End of Important JS -->
<div class="top">
    <table style="margin: 0px 15px 0px 15px;width: 99%"><tr><td width="30%" style="padding-top: 2px;text-align: left;font-size: 1.5em;color: white"><span class="glyphicon glyphicon-th"></span><span class="logo-lg"> SiABS Sekolah</span></td><td style="text-align:center;font-size: 2em;color: yellow"><span class="logo-lg">Daftar soal</span></td><td width="30%"  style="text-align: right;padding-right: 10px; font-size: 1.5em;color: white"><?php echo $_SESSION['surename'];?></td></tr></table>
</div>  
<div class="article">
<div class="box-body" style="width: 70%; margin: auto;">
<input type="hidden" id="uid_mapel" value="<?php echo $nama_pb['uid_mapel'];?>">
<input type="hidden" id="uid_pb" value="<?php echo $_REQUEST['uid_pb'];?>">
<table style="width: 100%">
    <tr><td width="200px">Jenjang / Mapel</td><td width="10px">&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><b><?php echo strtoupper($nama_pb['jenjang'])." / ".$nama_mapel['nama_mapel'];?></b></td><td rowspan="2" style="text-align: right;"><button class="btn btn-success" onclick="input_soal()"><span class="glyphicon glyphicon-plus-sign"></span> Tambah Soal Baru</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger"  onclick="daftar_close()"><span class="glyphicon glyphicon-remove"></span></button></td></tr>
    <tr><td>Pokok Bahasan/Subs</td><td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><b><?php echo $nama_pb['pb'];?></b></td></tr>
</table>
<table id="example3" class="table table-bordered">
      <thead  style='display:none;'>
        <tr><th>No.</th><th>Soal</th><th>Action</th></tr>
      </thead>
    <tbody>
          <?php
        $no=1;
        while($a=mysqli_fetch_array($q)){
          echo "
          <tr><td style='font-size:30px; width:100px;text-align:center;' valign='top'>".$no.".</td>
              <td valign='top'>";
                if($a['sound']=="ada"){
                    echo "<audio controls><source src='".$a['file_sound']."' type='audio/mpeg'>Your browser does not support the audio element.</audio><br>";
                }
                echo $a['soal']."
                <ol type='A'>
                  <li style='color:red;'>".$a['kunci']."</li>
                  <li>".$a['pengecoh_1']."</li>
                  <li>".$a['pengecoh_2']."</li>
                  <li>".$a['pengecoh_3']."</li>";
                  if($nama_pb['jenjang']=="sma"){
                    echo "<li>".$a['pengecoh_4']."</li>";
                  }
                echo "</ol>
              </td>
              <td style='width: 200px'>
                  <button class='btn btn-success'  id='".$a['uid_soal']."' onclick='edit_soal(this.id)'><span class='glyphicon glyphicon-edit'></span> Edit</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-danger'  id='".$a['uid_soal']."' onclick='delete_data(this.id)'><span class='glyphicon glyphicon-trash'></span> Hapus</button>
              </td> ";
          $no=$no+1;
        }
      ?>
    </tbody>
  </table>
</div><!-- /.box-body -->

<div class="footer" style="font-family: sans-serif;text-align: right;"><strong>&copy; <?php echo date('Y'); ?> Enlarge by <a href="#">Ismuji and A4 Team</a>.</strong> All rights reserved.</div>
<!-- DataTables -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
<script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
function delete_data(id){
      $.ajax({
      url: "proses_soal_hapus.php",
      type: "GET",
      dataType: 'html',
      data : {uid_soal: id,},
      success: function (ajaxData){
        window.location.reload();
      }
     })   
}
function edit_soal(id){
 window.open("blank_edit_soal.php?uid_soal="+id,"_self");
}
function input_soal(){
  var uid_mapel = document.getElementById("uid_mapel").value; 
  var uid_pb = document.getElementById("uid_pb").value; 
 window.open("input_soal.php?uid_mapel="+uid_mapel+"&uid_pb="+uid_pb,"_self");
}

function daftar_close(){
  document.location="../view/preview.php?view=pokok_bahasan";
}
</script>
    <script>
      $(function () { 
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#example3').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false,
          "pageLength": 1,
          "sDom": '<"top"p><"clear">',
          "drawCallback": function( settings ) { 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]) 
          }
        });
      });
    </script>
</body>
</html>
