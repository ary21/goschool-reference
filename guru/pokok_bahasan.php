<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
     <p  style="text-align:right;margin-right: 10px;"><a href="#" class="btn btn-primary modal_pb_add"><span class="glyphicon glyphicon-plus-sign"></span> Tambah Pokok Bahasan</a>&nbsp;&nbsp;&nbsp;</p>
       <div class="box-body">
      <table id="example4" class="table table-bordered table-striped">
        <thead>
          <tr>
<th style='width:20px'>No</th>
<th>Jenjang</th>
<th>Mata Pelajaran</th>
<th>Pokok Bahasan</th>
<th>Banyak Soal</th>
<th></th>
          </tr>
        </thead>
        <tbody>
      <?php
        $tampil=mysqli("SELECT t2.uid_mapel, t2.nama_mapel, t1.uid_pb, t1.pb, t1.jenjang FROM tb_pb t1 JOIN tb_mapel t2 ON t1.uid_mapel=t2.uid_mapel WHERE t1.uid_user='".$_SESSION['id_user']."' ORDER BY t1.jenjang, t2.nama_mapel ASC");
        $no = 1;
        while($r=mysqli_fetch_array($tampil)){
            $n_soal=mysqli_num_rows(mysqli("SELECT id_soal FROM tb_soal WHERE uid_pb='".$r['uid_pb']."'"));
        echo "<tr><td>$no</td><td>".strtoupper($r['jenjang'])."</td><td>$r[nama_mapel]</td><td>$r[pb]</td><td>".$n_soal."</td><td>
        <a class='btn btn-success  btn-xs modal_pb_edit' title='edit nama pb' id='".$r['uid_pb']."'><span class='glyphicon glyphicon-list'></span> Edit</a>&nbsp;&nbsp;&nbsp;
        <a href='../guru/input_soal.php?uid_mapel=$r[uid_mapel]&uid_pb=$r[uid_pb]&cat=new' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus-sign'></span> Tambah Soal</a>&nbsp;&nbsp;&nbsp;";
        if($n_soal==0){
            echo "<button class='btn btn-danger  btn-xs'  id='".$r['uid_pb']."' onclick='delete_data(this.id)'><span class='glyphicon glyphicon-trash'></span> Hapus</button>";
        }else{
            echo "<a href='../guru/blank_daftar_soal.php?uid_pb=$r[uid_pb]' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-folder-open'></span>  Lihat Soal</a>";
        }
        echo "</td></tr>";
          $no++;
          }
      ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
    </div>
</div>
<!--/MODAL -->
<div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
</div>
<!-- DELETE PB -->
<script type="text/javascript">
function delete_data(id){
    $.ajax({
      url: "../guru/proses_pb_hapus.php",
      type: "GET",
      dataType: 'html',
      data : {uid_pb: id,},
      success: function (ajaxData){
        window.location.reload();
      }
     })   
}
</script>
<!-- SCRIPT ADD PB -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_pb_add").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/modal_pb_add.php",
                        type: "GET",
                        data : {uid_pb: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>
<!-- SCRIPT EDIT PB -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_pb_edit").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/modal_pb_edit.php",
                        type: "GET",
                        data : {uid_pb: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>
<!-- SCRIPT HAPUS PB -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_pb_hapus").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/modal_pb_hapus.php",
                        type: "GET",
                        data : {uid_pb: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>        
<!-- SCRIPT SOAL -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_add_soal").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/add_soal.php",
                        type: "GET",
                        data : {uid_pb: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>        
