<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Penulisan Soal</title>
    <link rel="shortcut icon" href="../dist/img/favicon.png" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        tex2jax: {inlineMath: [['$','$'], ['\\(','\\)'],['<span class="math-tex">\\(','\\)</span>']]}
      });
    </script>
    <script type="text/javascript" async src="../plugins/MathJax/MathJax.js?config=TeX-MML-AM_CHTML"></script>    
</head>
<body style="background: #F7EFFF">
<?php
session_start();
include "../config/koneksi.php";
if(isset($_REQUEST['uid_mapel'])){
  $_SESSION['uid_mapel']=$_REQUEST['uid_mapel'];
}
if(isset($_REQUEST['uid_pb'])){
  $_SESSION['uid_pb']=$_REQUEST['uid_pb'];
}
$nama_mapel=mysqli_fetch_array(mysqli("SELECT nama_mapel FROM tb_mapel WHERE uid_mapel='".$_SESSION['uid_mapel']."'"));
$nama_pb=mysqli_fetch_array(mysqli("SELECT jenjang, pb FROM tb_pb WHERE uid_pb='".$_SESSION['uid_pb']."'"));
?>
<!-- Important JS to make this project works -->
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/jquery.deskform.js"></script>
<script type="text/javascript" src="js/jquery.migrate-1.2.1.min.js"></script>

<!-- End of Important JS -->
<div class="top">
    <table style="margin: 0px 15px 0px 15px;width: 99%"><tr><td width="30%" style="padding-top: 2px;text-align: left;font-size: 1.5em;color: white"><span class="glyphicon glyphicon-th"></span><span class="logo-lg"> SiABS Sekolah</span></td><td style="text-align:center;font-size: 2.2em;color: yellow"><span class="logo-lg"><b>Input soal</b></span></td><td width="30%"  style="text-align: right;padding-right: 10px; font-size: 1.5em;color: white"><?php echo $_SESSION['surename'];?></td></tr></table>
</div>  
<div class="article">
<table style="width: 100%">
    <tr><td width="200px">Jenjang/Mapel</td><td width="10px">&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><b><?php echo strtoupper($nama_pb['jenjang'])."/".$nama_mapel['nama_mapel'];?></b></td><td rowspan="2" style="text-align: right;"><button class="btn btn-success" id="<?php echo $_SESSION['uid_pb'];?>" onclick="daftar_soal(this.id)"><span class="glyphicon glyphicon-folder-open"></span> Daftar Soal</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger"  onclick="soal_close()"><span class="glyphicon glyphicon-remove"></span></button></td></tr>
    <tr><td>Pokok Bahasan/Subs</td><td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><b><?php echo $nama_pb['pb'];?></b></td></tr>
</table>

  <div style="width: 100%; overflow: hidden;">
    <div style="width: 49%; float: left;max-height: 600px;overflow-x: hidden;overflow-y: auto;">
        <h3>Source Code</h3>
        <table>
          <tr><td>Suara dalam soal</td><td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td>
          <input type="radio" name="add_sound" value="No" checked style="transform: scale(1.5);" onclick="show_off();">&nbsp;&nbsp;<b>Tidak</b>&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="radio" name="add_sound" value="Yes" style="transform: scale(1.5);" onclick="show_on();">&nbsp;&nbsp;<b>Ada</b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td><span id="get_sound"></span></td></tr>
        </table>
        <br>
        <table width="100%">
          <tr><td colspan="2"><textarea id="soal" name="soal" style="width:100%;height: 100px" onfocus="updateFocus(this)"></textarea></td><td width="40px" align="center"><button class="btn_edit edit" id="edit_soal" onclick="editTextarea(this)"><span class="glyphicon glyphicon-share"></span></button></td></tr>
          <tr><td valign="top" style="width: 80px;"><span class="btn btn-primary" style="width:60px;">Key</span></td><td><textarea id="key" name="key"  style="width:100%;height: 45px"  onfocus="updateFocus(this)"></textarea></td><td width="60px" align="center"><button class="btn_edit edit" id="edit_kunci" onclick="editTextarea(this)"><span class="glyphicon glyphicon-share"></span></button></td></tr>
          <tr><td valign="top"><span class="btn btn-primary" style="width:60px;">Dist1</span></td><td><textarea id="dist1" name="dist1"  style="width:100%;height: 45px"  onfocus="updateFocus(this)"></textarea></td><td width="40px" align="center"><button class="btn_edit edit" id="edit_dist1" onclick="editTextarea(this)"><span class="glyphicon glyphicon-share"></span></button></td></tr>
          <tr><td valign="top"><span class="btn btn-primary" style="width:60px;">Dist2</span></td><td><textarea id="dist2" name="dist2"  style="width:100%;height: 45px"  onfocus="updateFocus(this)"></textarea></td><td width="40px" align="center"><button class="btn_edit edit" id="edit_dist2" onclick="editTextarea(this)"><span class="glyphicon glyphicon-share"></span></button></td></tr>
          <tr><td valign="top"><span class="btn btn-primary" style="width:60px;">Dist3</span></td><td><textarea id="dist3" name="dist3"  style="width:100%;height: 45px"  onfocus="updateFocus(this)"></textarea></td><td width="40px" align="center"><button class="btn_edit edit" id="edit_dist3" onclick="editTextarea(this)"><span class="glyphicon glyphicon-share"></span></button></td></tr>
          <?php
          if ($nama_pb['jenjang']=="sma") {
            echo "<tr><td valign='top'><span class='btn btn-primary' style='width:60px;'>Dist4</span></td><td><textarea id='dist4' name='dist4'  style='width:100%;height: 45px'  onfocus='updateFocus(this)'></textarea></td><td width='40px' align='center'><button class='btn_edit edit' id='edit_dist4' onclick='editTextarea(this)'><span class='glyphicon glyphicon-share'></span></button></td></tr>";
          }else{
            echo "<input type='hidden' id='dist4' name='dist4' value=''>";   
          }            
          ?>
          <tr><td colspan="2" style="text-align: right;"><a href="" class="btn btn-default prev_soal" data-toggle="modal" id="1"><span class="glyphicon glyphicon-eye-open"></span> Preview</a>&nbsp;&nbsp;&nbsp;<button class="btn btn-primary" id="simpan" onclick="save_data()"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button></td><td></td></tr>
        </table>
    </div>

    <div style="width: 2%; float: left;max-height: 800px;overflow-x: hidden;overflow-y: auto;">&nbsp;
    </div>

<!--EDITOR-->
    <div style="width: 49%; float: left;max-height: 800px;overflow-x: hidden;overflow-y: auto;">
    <h3>Editor</h3>
      <table  width="100%">
        <tr><td valign="top"><textarea id="ckeditor1" name="description" class="form-control"></textarea></td></tr>
        <tr><td style="text-align: right;"><button class="btn btn-default store"><span class="glyphicon glyphicon-arrow-left"></span> Store</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="" class="btn btn-default upload-form-modal" data-toggle="modal"><span class="glyphicon glyphicon-open"></span> Image</a></td></tr>
        </table>
      </div>
 
 </div>
<div class="footer" style="font-family: sans-serif;text-align: right;"><strong>&copy; <?php echo date('Y'); ?> Enlarge by <a href="#">Ismuji and A4 Team</a>.</strong> All rights reserved.</div>
<!-- End of form -->
<!--/MODAL -->
    <div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left: 50%;">
    </div>
    <div class="modal fade" id="prev_soal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div>

    <div class="modal fade" id="modal_alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width:300px;height: 300px;">
        <div class="modal-content">
          <p><div id="message" style="font-size: 18px;text-align: center;font-weight: bold;color: blue;"></div></p>
          <p style="text-align: center;"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></p>
        </div>
      </div>
  </div>

<!-- SCRIPT ADD IMAGE -->
<script type="text/javascript">
  $(document).ready(function (){
    $(".upload-form-modal").click(function (e){
        var m = $(this).attr("id");
        $.ajax({
          url: "modal_image.php",
          type: "GET",
          data : {id: m,},
          success: function (ajaxData){
              $("#ModalUtama").html(ajaxData);
              $("#ModalUtama").modal('show',{backdrop: 'true'});
          }
        });
    });
  });
</script>

<!---SCRIPT SHOW/OFF-->
<script type="text/javascript">
  function show_off(){
    $("#get_sound").html("");
  }
  function show_on(){
    $("#get_sound").html("<input type='file' name='file' id='file'>");
  }
</script>

<!-- SCRIPT PREV SOAL --->
        <script>
            $(document).ready(function (){
                $(".prev_soal").click(function (e){
                  var add_sound = $("input[name='add_sound']:checked"). val();
                  var prev_soal=[];
                    prev_soal[0] = document.getElementById("soal").value;
                    prev_soal[1] = document.getElementById("key").value;
                    prev_soal[2] = document.getElementById("dist1").value;
                    prev_soal[3] = document.getElementById("dist2").value;
                    prev_soal[4] = document.getElementById("dist3").value;
                    prev_soal[5] = document.getElementById("dist4").value;
                    prev_soal[6] = add_sound;
                    $.ajax({
                        url: "modal_prevsoal.php",
                        type: "GET",
                        dataType: 'html',
                        data : {soal: prev_soal,},
                        success: function (ajaxData){
                            $("#prev_soal").html(ajaxData);
                            $("#prev_soal").modal('show',{backdrop: 'true'});
                            MathJax.Hub.Queue(["Typeset",MathJax.Hub,'prev_soal']);
                        }
                    });
                });

            });

    function updateFocus(x){el = x;}
    $(".store").click(function (e){
      var m = CKEDITOR.instances.ckeditor1.getData();
    if (el.setSelectionRange){
        el.value = el.value.substring(0,el.selectionStart) + m + el.value.substring(el.selectionStart,el.selectionEnd) +el.value.substring(el.selectionEnd,el.value.length);
    }
    clear_ckeditor();
  })

    function clear_ckeditor(){
      CKEDITOR.instances.ckeditor1.setData('');
    }
</script>

<script>
  function  editTextarea(obj){
    var m = obj.id;
        switch(m) {
        case 'edit_soal':
          var id_x="soal";
          break;
        case 'edit_kunci':
          var id_x="key";
          break;
        case 'edit_dist1':
          var id_x="dist1";
          break;
        case 'edit_dist2':
          var id_x="dist2";
          break;
        case 'edit_dist3':
          var id_x="dist3";
          break;
        case 'edit_dist4':
          var id_x="dist4";
          break;
        }
        var isi=document.getElementById(id_x).value;
        CKEDITOR.instances.ckeditor1.setData(isi);
        document.getElementById(id_x).value='';
      }

</script>
<script>
  function daftar_soal(id){
    window.open("blank_daftar_soal.php?uid_pb="+id,"_self");
  }
</script>

<script>
  function save_data(){
    var fd = new FormData();
    var add_sound = $("input[name='add_sound']:checked"). val();
    var soal = document.getElementById("soal").value;
    var key = document.getElementById("key").value;
    var dist1 = document.getElementById("dist1").value;
    var dist2 = document.getElementById("dist2").value;
    var dist3 = document.getElementById("dist3").value;
    var dist4 = document.getElementById("dist4").value;
    if(add_sound=="Yes"){
        fd.append('file',$('#file')[0].files[0]);
    }
    fd.append('soal', soal);
    fd.append('key', key);
    fd.append('dist1', dist1);
    fd.append('dist2', dist2);
    fd.append('dist3', dist3);
    fd.append('dist4', dist4);
    fd.append('sound', add_sound);
    $.ajax({
        method:"POST",
        url:"simpan_soal.php",    
        data: fd,  
        cache: false,
        contentType: false,
        processData: false,   
        success: function(data){
            if(data!="Soal Anda Berhasil disimpan!"){
                $("#message").html(data);
                $("#modal_alert").modal('show');
            }else{
                window.location.reload();            
            }
        }
    });
  }
</script>

<script type="text/javascript">
  function soal_close(){
    document.location="../view/preview.php?view=pokok_bahasan";
  }
</script>

<script>
    function myFunction-r() {
    var x = document.getElementById("soal").value;  
    var myWindow = window.open("", "", "width=400,height=400");
    myWindow.document.write(x);
    }
</script>
<!-- CKEditor -->
<script src="../plugins/ckeditor/ckeditor.js"></script>
<!-- CKEditor Adapters --> 
<script src="js/ckeditor/adapters/jquery.js"></script>
<!-- CKEditor Adapters --> 

<script data-sample="1">
  CKEDITOR.replace( 'ckeditor1', {
    extraPlugins: 'mathjax,justify',
    mathJaxLib: '../plugins/mathjax/MathJax.js?config=TeX-AMS_HTML',
    width:'100%',
    height:'300px',
  } );
</script>    
<!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- CKE Image Uploader JS -->
<script src="js/cke-upload-image.js"></script>
</body>
</html>
