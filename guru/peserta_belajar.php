<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
       <div class="box-body">
      <table id="example4" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style='width:20px' align="center">No</th>
            <th>Mata Pelajaran</th>
            <th>Topik</th>
            <th>PD</th>
            <th>Belum</th>
            <th>Sudah</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
      <?php
        $tampil=mysqli("
            SELECT t1.uid_material, t1.topic, t1.upload, t1.status, t1.reader, t2.nama_mapel
            FROM tb_material t1 
            JOIN tb_mapel t2 ON t1.uid_mapel=t2.uid_mapel
            WHERE t1.uid_user='".$_SESSION['id_user']."'
            ORDER BY t1.topic ASC");
        $no = 1;
        while($r=mysqli_fetch_array($tampil)){
            $n_pd=mysqli_num_rows(mysqli("SELECT id_learned FROM tb_learned WHERE uid_material='".$r['uid_material']."'"));
            $n_0=mysqli_num_rows(mysqli("SELECT id_learned FROM tb_learned WHERE uid_material='".$r['uid_material']."' AND open='0'"));
            $n_1=$n_pd-$n_0;
            echo "<tr>
            <td align='center'>".$no.".</td>
            <td>".$r['nama_mapel']."</td>
            <td>".$r['topic']."</td>
            <td>".$n_pd."</td>
            <td>".$n_0."</td>
            <td>".$n_1."</td>
            <td>
            <a href='preview.php?view=learner&data=".$r['uid_material']."' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-user'></span> Learner</a></td></tr>";
          $no++;
          }
      ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
    </div>
</div>
<!--/MODAL -->
<div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
</div>
