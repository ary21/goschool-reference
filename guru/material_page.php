<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
     <p  style="text-align:right;margin-right: 10px;"><a href="#" class="btn btn-primary modal_material_add"><span class="glyphicon glyphicon-plus-sign"></span> Tambah Topik</a>&nbsp;&nbsp;&nbsp;</p>
       <div class="box-body">
      <table id="example4" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style='width:20px'>No</th>
            <th>Matpel</th>
            <th>Topik</th>
            <th>Upload</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
      <?php
        $tampil=mysqli("
            SELECT t1.uid_material, t1.topic, t1.upload, t1.status, t1.reader, t2.nama_mapel
            FROM tb_material t1 
            JOIN tb_mapel t2 ON t1.uid_mapel=t2.uid_mapel
            WHERE t1.uid_user='".$_SESSION['id_user']."'
            ORDER BY t1.topic ASC");
        $no = 1;
        while($r=mysqli_fetch_array($tampil)){
          /*
            if($r['upload']!=0){
                $upload="<span class='btn label label-primary' id='1|".$r['uid_material']."' onclick='toggle_upload(this.id)'>Ada</span>";
            }else{
                $upload="<span class='btn label label-default' id='0|".$r['uid_material']."' onclick='toggle_upload(this.id)'>Tidak</span>";
            }
            */
            if($r['status']!=0){
                $status="<span class='btn label label-primary' id='1|".$r['uid_material']."' onclick='toggle_status(this.id)'>Open</span>";
            }else{
                $status="<span class='btn label label-default' id='0|".$r['uid_material']."' onclick='toggle_status(this.id)'>Close</span>";
            }
            echo "<tr>
                <td>".$no.".</td>
                <td style='white-space:nowrap;'>".$r['nama_mapel']."</td>
                <td>".$r['topic']."</td>
                <td>".$upload."</td>
                <td>".$status."</td>
                <td>
                <a href='../guru/material_write.php?uid_material=".$r['uid_material']."' class='btn btn-success  btn-xs'><span class='glyphicon glyphicon-list'></span> Edit</a>&nbsp;&nbsp;&nbsp;
                <a href='#' class='btn btn-primary btn-xs view_materi' data-toggle='modal' id='".$r['uid_material']."'><span class='glyphicon glyphicon-eye-open'></span> Prev</a>
                </td></tr>";
          $no++;
          }
      ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
    </div>
</div>
<!--/MODAL -->
<div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
</div>
<div class="modal fade" id="view_materi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<!--SCRIPT-->
<script type="text/javascript">
    $(document).ready(function (){
        $(".modal_material_add").click(function (e){
            $.ajax({
            url: "../guru/modal_material_add.php",
            type: "GET",
            success: function (ajaxData){
                $("#ModalUtama").html(ajaxData);
                $("#ModalUtama").modal('show',{backdrop: 'true'});
            }
            });
        });
    });
    function toggle_status(id){
        var data=id.split("|");
        $.ajax({
            url: "../guru/fungsi.php",
            type: "GET",
            dataType: 'html',
            data : {funct:"toggle_status",kode:data[0],uid_material:data[1]},
            success: function (ajaxData){
                window.location.reload();
            }
        })
    }
    function toggle_upload(id){
        var data=id.split("|");
        $.ajax({
            url: "../guru/fungsi.php",
            type: "GET",
            dataType: 'html',
            data : {funct:"toggle_upload",kode:data[0],uid_material:data[1]},
            success: function (ajaxData){
                window.location.reload();
            }
        })
    }
</script>
<script type="text/javascript">
  $(document).ready(function (){
    $(".view_materi").click(function (e){
        var m = $(this).attr("id");
        $.ajax({
          url: "../guru/modal_materi.php",
          type: "GET",
          dataType: 'html',
          data : {uid_material: m,},
          success: function (ajaxData){
              $("#view_materi").html(ajaxData);
              $("#view_materi").modal('show',{backdrop: 'true'});
              MathJax.Hub.Queue(["Typeset",MathJax.Hub,'view_materi']);
          }
        });
    });
  });
</script>

