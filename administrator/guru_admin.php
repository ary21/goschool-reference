<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
     <p  style="text-align:right;margin-right: 10px;"><a href="#" class="btn btn-primary modal_guru_add"><span class="glyphicon glyphicon-plus-sign"></span> Tambah Guru</a>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary modal_guru_aktif"><span class="glyphicon glyphicon-ok"></span> Aktifkan Guru</a>&nbsp;&nbsp;&nbsp;<a href="files/Format Input Data Guru.xlsx" class="btn btn-primary modal_mapel_add"><span class="glyphicon glyphicon-file"></span> Download Template</a>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary modal_guru_import"><span class="glyphicon glyphicon-open"></span> Upload from Excel</a></p>
     <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
<th style='width:15px'>No</th>
<th style='width:30px'>ID Guru</th>
<th>Nama Guru</th>
<th>Pass</th>
<th>Pembuat Soal</th>
          </tr>
        </thead>
        <tbody>
      <?php
        $tampil = mysqli("SELECT * FROM tb_users WHERE level='guru' AND status='aktif'");
        $no = 1;
        while($r=mysqli_fetch_array($tampil)){
        echo "<tr><td>".$no."</td>
      <td>".$r['id_number']."</td>
      <td>".$r['sure_name']."</td>
      <td>".$r['_password']."</td>
      <td>";
       $q2=mysqli("SELECT t2.nama_mapel FROM tb_guru_mapel t1 JOIN tb_mapel t2 ON t1.uid_mapel=t2.uid_mapel WHERE t1.uid_user='".$r['uid_user']."'");
      if(mysqli_num_rows($q2)>0){
          while ($a2=mysqli_fetch_array($q2)) {
            echo "<li>".$a2['nama_mapel']."</li>";
          }
        }
      echo "
      </td>
      <td></td>
      <td><a class='btn btn-success  btn-xs modal_edit_guru' title='edit guru' id='".$r['uid_user']."'><span class='glyphicon glyphicon-pencil'></span> Edit</a>&nbsp;&nbsp;&nbsp;<a class='btn btn-success  btn-xs modal_dist_guru' title='edit mapel guru' id='".$r['uid_user']."'><span class='glyphicon glyphicon-list'></span> Distribusi</a>&nbsp;&nbsp;&nbsp;<a class='btn btn-danger  btn-xs modal_nonaktif_guru' title='nonaktif_guru' id='".$r['uid_user']."'><span class='glyphicon glyphicon-trash'></span>  #NA</a></td>
  </tr>";
          $no++;
          }
      ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
    </div>
</div>
<!--/MODAL -->
    <div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div>
<!-- SCRIPT ADD MAPEL -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_guru_add").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/guru_add.php",
                        type: "GET",
                        data : {id_tes: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>

<!-- SCRIPT EDIT NAMA GURU -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_edit_guru").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/guru_edit.php",
                        type: "GET",
                        data : {uid_guru: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>

<!-- SCRIPT DISTRIBUSI GURU -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_dist_guru").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/guru_dist.php",
                        type: "GET",
                        data : {uid_guru: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>

<!-- SCRIPT NON AKTIF GURU -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_nonaktif_guru").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/guru_na.php",
                        type: "GET",
                        data : {uid_user: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>   
<!-- SCRIPT AKTIFKAN GURU -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_guru_aktif").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/guru_aktif.php",
                        type: "GET",
                        data : {uid_user: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>     

<!-- SCRIPT IMPORT GURU -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_guru_import").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/guru_import.php",
                        type: "GET",
                        data : {uid_user: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>                    
