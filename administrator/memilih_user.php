<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
    </div>
       <div class="box-body">
        <?php
            $tes=mysqli_fetch_array(mysqli("SELECT id_tes, jenjang, uid_tes, uid_user, uid_mapel, nama_tes, waktu, mode_ambil, mode_tampil, status_tes, update_tes FROM tb_tes WHERE uid_tes='".$data."'"));
        ?>
        <script type="text/javascript">
            $(document).ready(function(){ 
            $("#myTab a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
                });
            });
        </script>
<p><b><?php echo $tes['nama_tes'];?></b></p>
<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#pd_all">Daftar Peserta</a></li>
    <li><a href="#pd_test">Peserta Baru</a></li>
    <li><a href="#pd_ongoing">Peserta Sedang Tes</a></li>
    <li><a href="#pd_tested">Peserta Sudah Tes</a></li>
</ul> 

<div class="tab-content">
          <!--TAB 1-->
          <div id="pd_all"  class="tab-pane fade in active">
            <?php
              $qpd_kelas=mysqli("SELECT t2.sure_name, t3.nama_kelas, t1.uid_user FROM tb_rombel t1 JOIN tb_users t2 ON t1.uid_user=t2.uid_user JOIN tb_datakelas t3 ON t1.id_kelas=t3.id_kelas WHERE t2.uid_user NOT IN (SELECT uid_user FROM tb_datatested WHERE uid_tes='".$tes['uid_tes']."') ORDER BY t3.nama_kelas,t2.sure_name ASC");
            ?>
            <table id="example5" width="100%" class="table bordered">
              <thead>
              <tr><th>No.</th><th>Nama</th><th>Kelas</th><th style="width: 300px;"><input type="checkbox" onClick="toggle1(this)"/> All &nbsp;&nbsp;&nbsp;<button class="btn btn-success" onclick="input_testie()"><span class="glyphicon glyphicon-log-in"></span> Input Testie</button>&nbsp;&nbsp;&nbsp;<span id="loading1"></span></th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_kelas=mysqli_fetch_array($qpd_kelas)) {
                echo "<tr><td>".$no."</td><td>".$apd_kelas['sure_name']."</td><td>".$apd_kelas['nama_kelas']."</td>
                <td><input type='checkbox' name='pd_in' value='".$apd_kelas['uid_user']."'></td></tr>";
                $no++;
              }
              
              ?>
              </tbody>
            </table>
          </div>

          <!--TAB 2-->
          <div id="pd_test"  class="tab-pane fade">
            <?php
              $qpd_test=mysqli("SELECT t1.uid_tested, t2.sure_name, t4.nama_kelas FROM tb_datatested t1 JOIN tb_users t2 ON t1.uid_user=t2.uid_user JOIN tb_rombel t3 ON t1.uid_user=t3.uid_user JOIN tb_datakelas t4 ON t3.id_kelas=t4.id_kelas WHERE t1.uid_tes='".$tes['uid_tes']."' AND t1.status='not yet' ORDER BY t4.nama_kelas,t2.sure_name ASC");
            ?>
            <table id="example5" class="table table-bordered table-striped">
              <thead>
              <tr><th>No.</th><th>Nama</th><th>Kelas</th><th style="width: 300px;"><input type="checkbox" onClick="toggle2(this)"/> All &nbsp;&nbsp;&nbsp;<button class="btn btn-danger" onclick="outpdfromtest()"><span class="glyphicon glyphicon-plus-sign"></span> Out Testie</button>&nbsp;&nbsp;&nbsp;<span id="loading2">
              </th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_test=mysqli_fetch_array($qpd_test)) {
                echo "<tr><td>".$no."</td><td>".$apd_test['sure_name']."</td><td>".$apd_test['nama_kelas']."</td><td><input type='checkbox' name='pd_tested' value='".$apd_test['uid_tested']."'></td></tr>";
                $no++;
              }
              
              ?>
              </tbody>
            </table>            
          </div>

          <!--TAB 3-->
          <div id="pd_ongoing"  class="tab-pane fade">
            <?php
              $qpd_teston=mysqli("SELECT t1.uid_tested, t2.sure_name, t4.nama_kelas FROM tb_datatested t1 JOIN tb_users t2 ON t1.uid_user=t2.uid_user JOIN tb_rombel t3 ON t1.uid_user=t3.uid_user JOIN tb_datakelas t4 ON t3.id_kelas=t4.id_kelas WHERE t1.uid_tes='".$tes['uid_tes']."' AND t1.status='on going' ORDER BY t4.nama_kelas,t2.sure_name ASC");
            ?>
            <table id="example5" class="table table-bordered table-striped">
              <thead>
              <tr><th>No.</th><th>Nama</th><th>Kelas</th><th style="width: 300px;"></th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_teston=mysqli_fetch_array($qpd_teston)) {
                echo "<tr><td>".$no."</td><td>".$apd_teston['sure_name']."</td><td>".$apd_teston['nama_kelas']."</td></tr>";
                $no++;
              }
              
              ?>
              </tbody>
            </table>            
          </div>

          <!--TAB 4-->         
          <div id="pd_tested"  class="tab-pane fade">
            <?php
              $qpd_tested=mysqli("SELECT t1.uid_tested, t2.sure_name, t4.nama_kelas,t1.nilai, t1.durasi FROM tb_datatested t1 JOIN tb_users t2 ON t1.uid_user=t2.uid_user JOIN tb_rombel t3 ON t1.uid_user=t3.uid_user JOIN tb_datakelas t4 ON t3.id_kelas=t4.id_kelas WHERE t1.uid_tes='".$tes['uid_tes']."' AND t1.status='finish' ORDER BY t4.nama_kelas,t2.sure_name ASC");
            ?>
            <table id="example5" class="table table-bordered table-striped">
              <thead>
              <tr><th>No.</th><th>Nama</th><th>Kelas</th><th>Durasi</th><th align="right">Nilai</th><th>Reset</th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_tested=mysqli_fetch_array($qpd_tested)) {
                echo "<tr>
                <td>".$no."</td><td>".$apd_tested['sure_name']."</td>
                <td>".$apd_tested['nama_kelas']."</td>
                <td>".$apd_tested['durasi']."</td>
                <td align='right'>".$apd_tested['nilai']."</td>
                <td><button id='".$apd_tested['uid_tested']."' class='btn btn-warning' onclick='reset(this.id)'>Reset</button>&nbsp;&nbsp;&nbsp;<button id='".$apd_tested['uid_tested']."' class='btn btn-success' onclick='retest(this.id)'>Re-Test</button></td>
                </tr>";
                $no++;
              }
              
              ?>
              </tbody>
            </table>             
          </div>       
            
          </div>

        </div> 
</div>
</div><!-- /.box-body -->
</div>
</div>
<script type="text/javascript">
    function input_testie(){
      $("#loading1").html("<img src='../dist/img/ajax-loader-gif-6.gif' width='30px'> Wait ...");
     var uid_tes = "<?php echo $tes['uid_tes']; ?>";
     var uid_user = $('input[name=pd_in]:checked').map(function(){
                        return $(this).val();
                 }).get();
        $.ajax( {
        url: "../guru/fungsi.php?funct=input_testie&uid_tes="+uid_tes+"&uid_user="+uid_user,
        type: "GET",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
        $("#loading1").html("");
      }
     })   
     }
    //==================
    function outpdfromtest(){
    $("#loading2").html("<img src='../dist/img/ajax-loader-gif-6.gif' width='30px'> Wait ...");
     var uid_tested = $('input[name=pd_tested]:checked').map(function(){
                        return $(this).val();
                 }).get();
        $.ajax( {
        url: "../guru/fungsi.php?funct=outpdfromtest&uid_tested="+uid_tested,
        type: "GET",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
        $("#loading2").html("");
      }
     })   
    }

    function toggle1(source) {
        checkboxes = document.getElementsByName('pd_in');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    function toggle2(source) {
        checkboxes = document.getElementsByName('pd_tested');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    function retest(id){
      $.ajax( {
        url: "../guru/fungsi.php?funct=reset_tested&uid_tested="+id,
        type: "GET",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
    }

    function reset(id){
      $.ajax( {
        url: "../guru/fungsi.php?funct=reset_test&uid_tested="+id,
        type: "GET",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
    }

</script>