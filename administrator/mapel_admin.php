<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
     <p  style="text-align:right;margin-right: 10px;"><a href="#" class="btn btn-primary modal_mapel_add"><span class="glyphicon glyphicon-plus-sign"></span> Tambah Mapel</a></p>
       <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
<th style='width:20px'>No</th>
<th>Kode Pelajaran</th>
<th>Mata Pelajaran</th>
<th>Banyak Soal</th>
<th></th>
          </tr>
        </thead>
        <tbody>
      <?php
        $tampil = mysqli("SELECT * FROM tb_mapel");
        $no = 1;
        while($r=mysqli_fetch_array($tampil)){
        echo "<tr><td>$no</td>
      <td>$r[uid_mapel]</td>
      <td>$r[nama_mapel]</td>
      <td></td>
      <td><a class='btn btn-success  btn-xs modal_mapel_edit' title='edit nama mapel' id='".$r['uid_mapel']."'><span class='glyphicon glyphicon-list'></span> Edit</a>&nbsp;&nbsp;&nbsp;<a class='btn btn-danger  btn-xs modal_mapel_hapus' title='hapus mapel' id='".$r['uid_mapel']."'><span class='glyphicon glyphicon-trash'></span>  Hapus</a></td>
  </tr>";
          $no++;
          }
      ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
    </div>
</div>
<!--/MODAL -->
    <div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div>
<!-- SCRIPT ADD MAPEL -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_mapel_add").click(function (e){
                    $.ajax({
                        url: "../administrator/mapel_add.php",
                        type: "GET",
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>

<!-- SCRIPT EDIT MAPEL -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_mapel_edit").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/mapel_edit.php",
                        type: "GET",
                        data : {uid_mapel: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>

<!-- SCRIPT EDIT MAPEL -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_mapel_hapus").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/mapel_hapus.php",
                        type: "GET",
                        data : {uid_mapel: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>
