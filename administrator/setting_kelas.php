<div class="col-xs-12">
    <div class="box">
        <script type="text/javascript">
          $(document).ready(function(){ 
          $("#myTab a").click(function(e){
            e.preventDefault();
            $(this).tab('show');
          });
        });
        </script>
        <div class="box-header">
            
        </div>
       <div class="box-body">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#pd_kelas">Daftar PD pada Kelas</a></li>
            <li><a href="#add_pd">Tambah Pd pada Kelas</a></li>
            <li><a href="#add_kelas">Tambah Kelas</a></li>
        </ul>
        <div class="tab-content">
          <div id="pd_kelas"  class="tab-pane fade in active">
            <?php
              $qpd_kelas=mysqli("SELECT t2.sure_name, t2._username, t2._password, t3.nama_kelas, t1.uid_pd FROM tb_rombel t1 JOIN tb_users t2 ON t1.uid_user=t2.uid_user JOIN tb_datakelas t3 ON t1.id_kelas=t3.id_kelas ORDER BY t3.nama_kelas,t2.sure_name ASC");
            ?>
            <table id="example4" width="100%" class="table bordered">
              <thead>
              <tr><th>No.</th><th>Nama</th><th>Username</th><th>Password</th><th>Kelas</th><th><input type="checkbox" onClick="toggle(this)"/> All &nbsp;&nbsp;&nbsp;<button class="btn btn-danger" onclick="out_pd()"><span class="glyphicon glyphicon-log-out"></span> Keluarkan PD</button></th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_kelas=mysqli_fetch_array($qpd_kelas)) {
                echo "<tr><td>".$no."</td><td>".$apd_kelas['sure_name']."</td><td>".$apd_kelas['_username']."</td><td>".$apd_kelas['_password']."</td><td>".$apd_kelas['nama_kelas']."</td>
                <td><input type='checkbox' name='pd_out' value='".$apd_kelas['uid_pd']."'></td></tr>";
                $no++;
              }
              
              ?>
              </tbody>
            </table>
          </div>

          <!--TAB 2-->
          <div id="add_pd"  class="tab-pane fade">
            <?php
              $qpd_all=mysqli("SELECT uid_user, sure_name FROM tb_users WHERE level='siswa' AND status='aktif' AND uid_user NOT IN (SELECT uid_user FROM tb_rombel) ORDER BY _username ASC");
              $kelas=mysqli("SELECT id_kelas, nama_kelas FROM tb_datakelas ORDER BY nama_kelas ASC");
            ?>
            <table id="example5" class="table table-bordered table-striped">
              <thead>
              <tr><th>No.</th><th>Nama</th><th style="width: 300px;">
                <select class="form-control select2" id="kelas">
                <?php
                  while ($a_kelas=mysqli_fetch_array($kelas)) {
                    echo "<option value='".$a_kelas['id_kelas']."'>".$a_kelas['nama_kelas']."</option>";
                  }
                ?>
                </select>
                <button class="btn btn-primary" onclick="add_pd()"><span class="glyphicon glyphicon-plus-sign"></span> Tambahkan PD</button>
              </th></tr>
              </thead>
              <tbody>
              <?php
              $no=1;
              while ($apd_all=mysqli_fetch_array($qpd_all)) {
                echo "<tr><td>".$no."</td><td>".$apd_all['sure_name']."</td><td style='text-align: center;'><input type='checkbox' name='pd' value='".$apd_all['uid_user']."'></td></tr>";
                $no++;
              }
              
              ?>
              </tbody>
            </table>            
          </div>

          <!--TAB 3-->         
          <div id="add_kelas"  class="tab-pane fade">
            <p align="center"><input type="text" class="form-control" id="nama_kelas" width="80px">
                <button class="btn btn-primary"  onclick="add_kelas()"><span class="glyphicon glyphicon-plus-sign"></span> Tambah Kelas</button></p>
          </div>

        </div> 
       </div><!-- /.box-body -->
   </div>
</div>
<script type="text/javascript">
  function add_kelas(){
    var nama_kelas=document.getElementById('nama_kelas').value;
    $.ajax({
      url: "../administrator/fungsi.php?funct=add_kelas&nama_kelas="+nama_kelas,
      type: "GET",
      success: function (ajaxData){
        alert(ajaxData);
      }
    });
  }

  function add_pd(){
    var kelas=document.getElementById('kelas').value;
    var uid_user = $('input[name=pd]:checked').map(function(){
                        return $(this).val();
                 }).get();
    $.ajax({
      url: "../administrator/fungsi.php?funct=pd_rombel&kelas="+kelas+"&uid_user="+uid_user,
      type: "GET",
      success: function (ajaxData){
        window.location.reload();
      }
    });
  }

  function out_pd(){
    var uid_pd = $('input[name=pd_out]:checked').map(function(){
                        return $(this).val();
                 }).get();
    $.ajax({
      url: "../administrator/fungsi.php?funct=pdrombel_out&uid_pd="+uid_pd,
      type: "GET",
      success: function (ajaxData){
        window.location.reload();
      }
    });
  }

  function toggle(source) {
    checkboxes = document.getElementsByName('pd_out');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
  }
</script>