  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Menambah Mata Pelajaran</b></h4>
      </div>
      <form action='../administrator/fungsi.php' method='POST'>
      <div class="modal-body"> 	
        <input type="hidden" name="funct" value="add_mapel">
        <table><tr><td>Mapel yang akan ditambah:&nbsp;&nbsp;</td><td><input type="text" name="nama_mapel" class="form-control" required="required" style="width:200px;" autofocus></td></tr></table>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  	</form>
    </div>
  </div>
