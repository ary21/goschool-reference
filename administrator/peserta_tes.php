<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
       <div class="box-body">
      <table id="example4" class="table table-bordered table-striped">
        <thead>
          <tr>
<th style='width:20px' align="center">No</th>
<th>Jenjang/Mata Pelajaran</th>
<th>Nama tes</th>
<th>PD</th>
<th>PD belum Tes</th>
<th>PD sedang Tes</th>
<th>PD sudah Tes</th>
<th style="width: 100px;"></th>
          </tr>
        </thead>
        <tbody>
      <?php
        $tampil=mysqli("SELECT uid_tes, jenjang, uid_mapel, nama_tes, waktu, mode_ambil, mode_tampil, status_tes, token, status_review, update_tes FROM tb_tes ORDER BY status_tes DESC, update_tes DESC LIMIT 20");
        $no = 1;
        while($r=mysqli_fetch_array($tampil)){
            $mapel=mysqli_fetch_array(mysqli("SELECT nama_mapel FROM tb_mapel WHERE uid_mapel='".$r['uid_mapel']."'"));
            $n_pd=mysqli_num_rows(mysqli("SELECT id_datatested FROM tb_datatested WHERE uid_tes='".$r['uid_tes']."'"));
            $bt_pd=mysqli_num_rows(mysqli("SELECT id_datatested FROM tb_datatested WHERE uid_tes='".$r['uid_tes']."' AND status='not yet'"));
            $st_pd=mysqli_num_rows(mysqli("SELECT id_datatested FROM tb_datatested WHERE uid_tes='".$r['uid_tes']."' AND status='on going'"));
            $ft_pd=mysqli_num_rows(mysqli("SELECT id_datatested FROM tb_datatested WHERE uid_tes='".$r['uid_tes']."' AND status='finish'"));
            echo "<tr>
            <td align='center'>".$no.".</td>
            <td><font color='red'>".$r['jenjang'].":<br>".$mapel['nama_mapel']."</td>
            <td>".$r['nama_tes']."</td>
            <td>".$n_pd."</td>
            <td>".$bt_pd."</td>
            <td>".$st_pd."</td>
            <td>".$ft_pd."</td>
            <td>
            <a href='preview.php?view=memilih_user&data=".$r['uid_tes']."' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-user'></span> Testie</a></td></tr>";
          $no++;
          }
      ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
    </div>
</div>
<!--/MODAL -->
<div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
</div>
<!-- DELETE TES -->
<script type="text/javascript">
function delete_data(id){
    $.ajax({
      url: "../guru/fungsi.php?funct=del_tes&uid_tes="+id,
      type: "GET",
      dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
}
</script>
<!-- SCRIPT ADD TES -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_tes_add").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/modal_tes_add.php",
                        type: "GET",
                        data : {uid_pb: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>
<!-- SCRIPT EDIT PB -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_tes_edit").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/modal_tes_edit.php",
                        type: "GET",
                        data : {uid_tes: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>
      
<!-- SCRIPT SOAL -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_add_soal").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../guru/add_soal.php",
                        type: "GET",
                        data : {uid_pb: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>        
