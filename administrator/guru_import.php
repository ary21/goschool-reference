  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Import Data Guru</b></h4>
      </div>
      <form action="../administrator/fungsi.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="funct" value="import_guru">
        <div class="modal-body"> 
          <table>
              <tr><td>Pilih File Data Guru</td><td>:</td><td><input type="file" name="file" required></td></tr>
              </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Import</button>
        </div>
  	   </form>
    </div>
  </div>
