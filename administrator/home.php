            <a style='color:#000' href='index.php?view=siswa'>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><span class="glyphicon glyphicon-user"></span></span>
                <div class="info-box-content">
                <?php $siswa = mysqli_fetch_array(mysqli("SELECT count(*) as total FROM tb_users WHERE level='siswa' AND status='aktif'")); ?>
                  <span class="info-box-text">Siswa</span>
                  <span class="info-box-number"><?php echo $siswa[total]; ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            </a>

            <a style='color:#000' href='index.php?view=guru'>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><span class="glyphicon glyphicon-user"></span></span>
                <div class="info-box-content">
                <?php $guru = mysqli_fetch_array(mysqli("SELECT count(*) as total FROM tb_users WHERE level='guru'")); ?>
                  <span class="info-box-text">Guru</span>
                  <span class="info-box-number"><?php echo $guru[total]; ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            </a>

            <a style='color:#000' href='index.php?view=bahantugas'>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><span class=" glyphicon glyphicon-book"></span></span>
                <div class="info-box-content">
                <?php $mapel = mysqli_num_rows(mysqli("SELECT   id_mapel FROM tb_mapel")); ?>
                  <span class="info-box-text">Mapel</span>
                  <span class="info-box-number"><?php echo $mapel; ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            </a>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><span class="glyphicon glyphicon-check"></span></span>
                <div class="info-box-content">
                <?php $soal = mysqli_num_rows(mysqli("SELECT uid_soal FROM tb_soal")); ?>
                  <span class="info-box-text">Soal</span>
                  <span class="info-box-number"><?php echo $soal; ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
