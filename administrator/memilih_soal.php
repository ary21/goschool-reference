<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
    </div>
       <div class="box-body">
        <?php
            $tes=mysqli_fetch_array(mysqli("SELECT id_tes, uid_tes, uid_user, uid_mapel, nama_tes, waktu, mode_ambil, mode_tampil, status_tes, update_tes FROM tb_tes WHERE uid_tes='".$data."'"));
        ?>
        <script type="text/javascript">
            $(document).ready(function(){ 
            $("#myTab a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
                });
            });
        </script>

<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#soal_tes">Soal Tes: <?php echo $tes['nama_tes'];?></a></li>
    <li><a href="#bank_soal">Bank Soal</a></li>
</ul> 

<div class="tab-content">

<div id="soal_tes"  class="tab-pane fade in active">
    <div style="max-height: 360px;overflow-x: hidden;overflow-y: scroll;">
    <?php
    $soaltes=mysqli("SELECT uid_soaltes, uid_tes, uid_soal, layout_soal, n_opsi, no_soal FROM tb_soaltes WHERE uid_tes='".$data."' ORDER BY id_soaltes ASC");
    $no=1;
    while($a_soaltes=mysqli_fetch_array($soaltes)){
        $soal1=mysqli_fetch_array(mysqli("SELECT video, sound, soal, kunci, pengecoh_1, pengecoh_2, pengecoh_3, pengecoh_4 FROM tb_soal WHERE uid_soal='".$a_soaltes['uid_soal']."'"));
            echo "<table width='100%'>";
            echo "
            <tr><td width='60px' valign='top' style='font-size:24px;color:red;'><b>".$no.".</b><br><button class='btn btn-danger  btn-xs'  id='".$a_soaltes['uid_soaltes']."' onclick='del_soaltes(this.id)'><span class='glyphicon glyphicon-remove'></span></button></td>
                <td>".$soal1['soal']."
                <ol type='A'>
                <li style='color:red;'><b>".$soal1['kunci']."</b></li>
                <li>".$soal1['pengecoh_1']."</li>
                <li>".$soal1['pengecoh_2']."</li>
                <li>".$soal1['pengecoh_3']."</li>
                <li>".$soal1['pengecoh_4']."</li>
                </ol>
                </td>
            <tr><td colspan='2'>
            <hr></td></tr>";
            $no=$no+1;
            }
        ?>
        </table>
    </div>
</div>

<div id="bank_soal" class="tab-pane fade">
<div style="max-height: 360px;overflow-x: hidden;overflow-y: scroll;">
    <?php
        $soal=mysqli("SELECT id_soal, uid_soal, uid_mapel, uid_pb, uid_user, video, sound, soal, kunci, pengecoh_1, pengecoh_2, pengecoh_3, pengecoh_4, update_soal FROM tb_soal WHERE uid_mapel='".$tes['uid_mapel']."' AND uid_user='".$tes['uid_user']."' AND uid_soal NOT IN (SELECT uid_soal FROM tb_soaltes WHERE uid_tes='".$data."') ORDER BY update_soal DESC");
    ?>
    <table id="example4" class="table bordered" width="100%">
<thead><tr><th width="40px" align='center'><button class='btn btn-success' onclick='add_soaltes()'><span class='glyphicon glyphicon-arrow-left'></span> Upload Soal Tes</button></th><th>Soal</th></tr></thead>
<tbody>
<?php

WHILE($a_soal=mysqli_fetch_array($soal)){
    $pb=mysqli_fetch_array(mysqli("SELECT pb FROM tb_pb WHERE uid_pb='".$a_soal['uid_pb']."'"));
    echo "<tr>
    <td valign='top' align='center'><input type='checkbox' name='soal_tes' value='".$a_soal['uid_soal']."'></td>
    <td><font color='blue'><b>".$pb['pb'].":</b></font>
    <br>".$a_soal['soal']."
    <ol type='A'>
                <li style='color:red;'><b>".$a_soal['kunci']."</b></li>
                <li>".$a_soal['pengecoh_1']."</li>
                <li>".$a_soal['pengecoh_2']."</li>
                <li>".$a_soal['pengecoh_3']."</li>
                <li>".$a_soal['pengecoh_4']."</li>
                </ol>
    </td></tr>";
}

?>
</tbody>
    </table>
</div>
</div>

</div>
</div><!-- /.box-body -->
</div>
</div>
<script type="text/javascript">
    function add_soaltes(){
     var uid_tes = "<?php echo $data; ?>";
     var uid_soal = $('input[name=soal_tes]:checked').map(function(){
                        return $(this).val();
                 }).get();
        $.ajax( {
        url: "../guru/fungsi.php?funct=add_soaltes&uid_tes="+uid_tes+"&uid_soal="+uid_soal,
        type: "POST",
        dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
     }
    //==================
    function del_soaltes(uid_soaltes){
      $.ajax({
      url: "../guru/fungsi.php?funct=del_soaltes&uid_soaltes="+uid_soaltes,
      type: "POST",
      dataType: 'html',
      success: function (ajaxData){
        window.location.reload();
      }
     })   
    }

</script>