<!DOCTYPE html>
<html>
<head>
	<title>Grafik</title>
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
<h1>Grafik</h1>
<script src="highcharts.js"></script>
<?php
$mapel=array("Agama","Pkn","Bin","Mat Wajib","Sejarah Indonesia","bahasa Inggris");
$data1=array(rand()*100,rand()*100,rand()*100,rand()*100,rand()*100,rand()*100);
$data2=array(rand()*100,rand()*100,rand()*100,rand()*100,rand()*100,rand()*100);
$data3=array(rand()*100,rand()*100,rand()*100,rand()*100,rand()*100,rand()*100);
$data4=array(rand()*100,rand()*100,rand()*100,rand()*100,rand()*100,rand()*100);
?>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">
var cat=<?php echo "['".join("','",$mapel)."']";?>;
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Monthly Average Rainfall'
    },
    subtitle: {
        text: 'Source: WorldClimate.com'
    },
    xAxis: {
        categories: cat,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (mm)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: <?php echo "'".$mapel[0]."'";?>,
        data: [<?php echo join(',',$data1); ?>]

    }, {
        name: 'New York',
        data: [<?php echo join(',',$data2); ?>]

    }, {
        name: 'London',
        data: [<?php echo join(',',$data3); ?>]

    }, {
        name: 'Berlin',
        data: [<?php echo join(',',$data4); ?>]

    }]
});
</script>
</body>
</html>