<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
     <p  style="text-align:right;margin-right: 10px;"><a href="#" class="btn btn-primary modal_pd_add"><span class="glyphicon glyphicon-plus-sign"></span> Tambah PD</a>&nbsp;&nbsp;&nbsp;<a href="preview.php?view=setting_kelas" class="btn btn-success setting_kelas"><span class="glyphicon glyphicon-plus-sign"></span> Setting Kelas</a>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary modal_pd_aktif"><span class="glyphicon glyphicon-ok"></span> Aktifkan PD</a>&nbsp;&nbsp;&nbsp;<a href="../files/Format Input Data PD.xlsx" class="btn btn-primary"><span class="glyphicon glyphicon-file"></span> Download Template</a>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary modal_pd_import"><span class="glyphicon glyphicon-open"></span> Upload from Excel</a></p>
     <div class="box-body">
      <table id="example6" class="table table-bordered table-striped">
        <thead>
          <tr>
<th style='width:20px'>No</th>
<th>ID PD</th>
<th>Nama Peserta Didik</th>
<th>Password</th>
<th>Jenjang</th>
<th></th>
          </tr>
        </thead>
        <tbody>
      <?php
        $tampil = mysqli("SELECT * FROM tb_users WHERE level='siswa' AND status='aktif' ORDER BY jenjang, sure_name ASC");
        $no = 1;
        while($r=mysqli_fetch_array($tampil)){
        echo "<tr><td>$no</td>
      <td>$r[id_number]</td>
      <td>$r[sure_name]</td>
      <td>$r[_password]</td>
      <td>$r[jenjang]</td>
      <td><a class='btn btn-success  btn-xs modal_edit_pd' title='edit pd' id='".$r['uid_user']."'><span class='glyphicon glyphicon-pencil'></span> Edit</a>&nbsp;&nbsp;&nbsp;<a class='btn btn-danger  btn-xs modal_nonaktif_pd' title='nonaktif_pd' id='".$r['uid_user']."'><span class='glyphicon glyphicon-trash'></span>  #NA</a></td>
  </tr>";
          $no++;
          }
      ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
    </div>
</div>
<!--/MODAL -->
    <div class="modal fade" id="ModalUtama" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div>
<!-- SCRIPT ADD MAPEL -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_pd_add").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/pd_add.php",
                        type: "GET",
                        data : {id_tes: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>

<!-- SCRIPT EDIT NAMA pd -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_edit_pd").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/pd_edit.php",
                        type: "GET",
                        data : {uid_pd: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>

<!-- SCRIPT DISTRIBUSI pd -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_dist_pd").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/pd_dist.php",
                        type: "GET",
                        data : {uid_pd: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>

<!-- SCRIPT NON AKTIF pd -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_nonaktif_pd").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/pd_na.php",
                        type: "GET",
                        data : {uid_user: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>   
<!-- SCRIPT AKTIFKAN pd -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_pd_aktif").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/pd_aktif.php",
                        type: "GET",
                        data : {uid_user: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>     

<!-- SCRIPT IMPORT pd -->
        <script type="text/javascript">
            $(document).ready(function (){
                $(".modal_pd_import").click(function (e){
                    var m = $(this).attr("id");
                    $.ajax({
                        url: "../administrator/pd_import.php",
                        type: "GET",
                        data : {uid_user: m,},
                        success: function (ajaxData){
                            $("#ModalUtama").html(ajaxData);
                            $("#ModalUtama").modal('show',{backdrop: 'true'});
                        }
                    });
                });
            });
        </script>                    
